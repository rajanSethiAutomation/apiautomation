package RestAssured.Search;

import com.fabhotels.Controller.searchApi_Date_business_Logic;
import com.fabhotels.Controller.searchApi_DeviceType_business_Logic;
import com.fabhotels.common.helper.data.TestDataHelper;
import com.fabhotels.common.helper.report.ReportHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

/*
Created By : Sethi,Rajan : 24/07/18
 */

public class searchApi_DeviceType extends ReportHelper {

    final static String First_Server = "fabhotels_uat";
    final static String Second_Server = "fabhotels_prod";

    searchApi_DeviceType_business_Logic logic_Class = new searchApi_DeviceType_business_Logic();

    @Test(dataProvider = "ExcelDataProvider", dataProviderClass = TestDataHelper.class, enabled = true)
    public void searchDeviceType(String roomCount, String noOfPax, String startDate, String endDate, String distance,
                                   String showInvisibleProperties, String sortOrder, String requestType, String responseType,
                                   String devicetype, String pageType, String sessionID, String dateLess, String mealPlan, String membershipLevel) {
        Boolean final_result = logic_Class.validateTestCase(First_Server,Second_Server,roomCount, noOfPax, startDate, endDate, distance, showInvisibleProperties, sortOrder, requestType, responseType,  devicetype, pageType, sessionID, dateLess, mealPlan, membershipLevel);
        Assert.assertTrue(final_result, " API failure");
    }
}