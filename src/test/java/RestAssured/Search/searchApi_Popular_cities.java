package RestAssured.Search;

import com.fabhotels.Controller.searchApi_Date_business_Logic;
import com.fabhotels.Controller.searchApi_PopularCities_business_Logic;
import com.fabhotels.common.helper.data.TestDataHelper;
import com.fabhotels.common.helper.report.ReportHelper;
import org.testng.Assert;
import org.testng.annotations.Test;
/*
Created By : Sethi,Rajan : 24/07/18
 */

public class searchApi_Popular_cities extends ReportHelper {

    final static String First_Server = "fabhotels_uat";
    final static String Second_Server = "fabhotels_prod";

    searchApi_PopularCities_business_Logic logic_Class = new searchApi_PopularCities_business_Logic();

    @Test(dataProvider = "ExcelDataProvider", dataProviderClass = TestDataHelper.class, enabled = true)
    public void search_Popular_cities(String lat,String Long, String distance, String minprice, String maxprice, String nearByRequired,
                                   String noOfPax, String sortOrder, String mealPlan, String preAppliedPriceRequired, String membershipLevel,
                                   String roomCount, String startDate, String endDate, String showAllProperties, String showInvisibleProperties,String requestType , String responseType,String deviceType,String pageType,String requestId,String sessionId,String dateLess) {


        Boolean final_result = logic_Class.validateTestCase(First_Server,Second_Server, lat, Long,  distance,  minprice,  maxprice,  nearByRequired,
                 noOfPax,  sortOrder,  mealPlan,  preAppliedPriceRequired,  membershipLevel,
                 roomCount,  startDate,  endDate,  showAllProperties,  showInvisibleProperties, requestType ,  responseType, deviceType, pageType, requestId, sessionId, dateLess);
        Assert.assertTrue(final_result, " API failure");
    }
}

