//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fabhotels.Validator;

import com.fabhotels.common.helper.common.ApiHelper;
import com.fabhotels.common.helper.report.ComparatorReportGenerator;
import com.fabhotels.common.helper.report.ReportHelper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonDiff;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;
import org.testng.Reporter;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

public class ApiResponseValidator {
    public ApiResponseValidator() {
    }

    public static boolean compareJson(String responseExpected, String responseActual, String propFilePath) {
        boolean comparisonResult = compareJsonWithRequest(responseExpected, responseActual, null, propFilePath);
        return comparisonResult;
    }

    public static boolean compareJsonWithRequest(String responseExpected, String responseActual, String request, String propFilePath) {
        ReportHelper reporter = new ReportHelper();
        ComparatorReportGenerator compareHtmlObj = new ComparatorReportGenerator();

        try {
            String compareFilePath = null;
            String linkCompareFilePath = null;
            if (request == null) {
                compareFilePath = compareHtmlObj.createCompartarReport();
            } else {
                compareFilePath = compareHtmlObj.createCompartarReportWithReq(request);
            }

            linkCompareFilePath = "./comparatorReport/" + compareFilePath.split("comparatorReport")[1];
            int differenceCounter = 0;
            ObjectMapper objMapper = new ObjectMapper();
            JsonNode responseExp = objMapper.readTree(responseExpected);
            JsonNode responseAct = objMapper.readTree(responseActual);
            JsonNode jsonDiff = JsonDiff.asJson(responseExp, responseAct);
            Set<String> dndCompareValues = null;
            if (propFilePath != null && !propFilePath.isEmpty()) {
                dndCompareValues = ApiHelper.getAllProperty(propFilePath);
            }

            for(int diffCount = 0; diffCount < jsonDiff.size(); ++diffCount) {
                String diffPath = jsonDiff.get(diffCount).get("path").asText();
                if (dndCompareValues == null || !dndCompareValues.contains(diffPath)) {
                    ++differenceCounter;
                    String operationType = jsonDiff.get(diffCount).get("op").asText();
                    JsonNode diffValueJsonNode = jsonDiff.get(diffCount).get("value");
                    String diffValue = null;
                    if (diffValueJsonNode != null) {
                        diffValue = jsonDiff.get(diffCount).get("value").asText();
                    }

                    String parentValue = "";
                    if (operationType.equalsIgnoreCase("replace") || operationType.equalsIgnoreCase("remove")) {
                        parentValue = responseExp.at(diffPath).asText();
                    }

                    logFailure(operationType.toUpperCase(), diffPath, diffValue, parentValue);
                    logFailureInReporter(compareFilePath, operationType.toUpperCase(), diffPath, diffValue, parentValue);
                }
            }

            if (differenceCounter > 0) {
                compareHtmlObj.appendFinalHTMLReport(compareFilePath);
                String differenceHTMLLink = "<a href='" + linkCompareFilePath + "'>Differences Link</a>";
                reporter.reporterLogging(false, differenceHTMLLink);
                return false;
            }
        } catch (IOException var20) {
            var20.printStackTrace();
        }

        return true;
    }

    public static boolean validateJsonSchema(String response, String responseSchema, String esFieldName) {
        boolean schemaValidationFlag = false;
        ReportHelper reporter = new ReportHelper();
        ComparatorReportGenerator compareHtmlObj = new ComparatorReportGenerator();

        try {
            String compareFilePath = null;
            String linkCompareFilePath = null;
            compareFilePath = compareHtmlObj.createCompartarReport();
            linkCompareFilePath = "./comparatorReport/" + compareFilePath.split("comparatorReport")[1];
            JsonNode jsonSchema = JsonLoader.fromString(responseSchema);
            JsonNode jsonResponse = JsonLoader.fromString(response);
            JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
            JsonValidator schemaValidator = factory.getValidator();
            ProcessingReport errorReport = schemaValidator.validate(jsonSchema, jsonResponse);
            String errorText = "Schema validation failures - in " + esFieldName + " :";
            if (!errorReport.isSuccess()) {
                Iterator var14 = errorReport.iterator();

                while(var14.hasNext()) {
                    ProcessingMessage msg = (ProcessingMessage)var14.next();
                    if (msg.getLogLevel().toString().equalsIgnoreCase("error")) {
                        schemaValidationFlag = false;
                        errorText = errorText + msg.getMessage() + "<br/>";
                    }
                }

                Reporter.log(errorText);
                logFailureInReporter(compareFilePath, "ES log comparison", "-", errorText, "-");
                compareHtmlObj.appendFinalHTMLReport(compareFilePath);
                String differenceHTMLLink = "<a href='" + linkCompareFilePath + "'>Differences Link</a>";
                reporter.reporterLogging(false, differenceHTMLLink);
            } else {
                schemaValidationFlag = true;
            }
        } catch (IOException var16) {
            Reporter.log("Couldn't convert schema because of error " + var16.getLocalizedMessage());
        } catch (ProcessingException var17) {
            Reporter.log("Couldn't process report " + var17.getLocalizedMessage());
        }

        return schemaValidationFlag;
    }

    public static boolean validateJsonObject(Object responseObj, String esFieldName) {
        if (responseObj.equals(null)) {
            Reporter.log("<br/><font size='2'>ES Logs field " + esFieldName + " is null.</font>");
            return false;
        } else {
            return true;
        }
    }

    private static void logFailureInReporter(String compareFilePath, String operationType, String differencePath, String differenceValue, String parentValue) {
        ComparatorReportGenerator compareObj = new ComparatorReportGenerator();

        try {
            compareObj.appendresultHTMLReport(compareFilePath, "<textarea disabled rows='7' cols='55'>" + operationType + "</textarea>", "<textarea disabled rows='7' cols='55'>" + differencePath + "</textarea>", "<textarea disabled rows='7' cols='55'>" + parentValue + "</textarea>", "<textarea disabled rows='7' cols='55'>" + differenceValue + "</textarea>");
        } catch (IOException var7) {
            var7.printStackTrace();
        }

    }

    private static void logFailure(String operationType, String path, String qaEnvValue, String liveEnvValue) {
        Reporter.log("<font size='2'><b>Difference Type - </b>" + operationType + "</font>");
        Reporter.log("<font size='1'><b>Jpath - </b>" + path + "</font>");
        Reporter.log("<font size='1'><b>QA Env value - </b>" + qaEnvValue + "</font>");
        Reporter.log("<font size='1'><b>Live Env value - </b>" + liveEnvValue + "</font>");
        Reporter.log("<br/>");
    }
}
