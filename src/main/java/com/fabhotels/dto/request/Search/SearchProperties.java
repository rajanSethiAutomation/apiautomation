
package com.fabhotels.dto.request.Search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchProperties {


    @SerializedName("latLong")
    @Expose
    public String latLong;


    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @SerializedName("cityName")
    @Expose
    public String cityName;

    public Boolean getPreAppliedPriceRequired() {
        return preAppliedPriceRequired;
    }

    public void setPreAppliedPriceRequired(Boolean preAppliedPriceRequired) {
        this.preAppliedPriceRequired = preAppliedPriceRequired;
    }

    @SerializedName("preAppliedPriceRequired")
    @Expose
    public Boolean preAppliedPriceRequired;

    public String getLocalityAddress() {
        return localityAddress;
    }

    public void setLocalityAddress(String localityAddress) {
        this.localityAddress = localityAddress;
    }

    @SerializedName("localityAddress")
    @Expose

    public String localityAddress;

    @SerializedName("localityIds")
    @Expose
    public List<Integer> localityIds = null;

    @SerializedName("nearByRequired")
    @Expose
    public Boolean nearByRequired;


    public NearPlaceFilter getNearPlaceFilter() {
        return nearPlaceFilter;
    }

    public void setNearPlaceFilter(NearPlaceFilter nearPlaceFilter) {
        this.nearPlaceFilter = nearPlaceFilter;
    }

    @SerializedName("nearPlaceFilter")
    @Expose

    private NearPlaceFilter nearPlaceFilter;


    @SerializedName("priceRangeMin")
    @Expose
    public Integer priceRangeMin;

    @SerializedName("priceRangeMax")
    @Expose
    public Integer priceRangeMax;


    @SerializedName("roomCount")
    @Expose
    public Integer roomCount;
    @SerializedName("noOfPax")
    @Expose
    public Integer noOfPax;
    @SerializedName("startDate")
    @Expose
    public String startDate;
    @SerializedName("endDate")
    @Expose
    public String endDate;
    @SerializedName("distance")
    @Expose
    public Integer distance;
    @SerializedName("distanceBucket")
    @Expose
    public List<Integer> distanceBucket = null;


    public List<Integer> getAmenitiesFilter() {
        return amenitiesFilter;
    }

    public void setAmenitiesFilter(List<Integer> amenitiesFilter) {
        this.amenitiesFilter = amenitiesFilter;
    }

    @SerializedName("amenitiesFilter")
    @Expose

    public List<Integer> amenitiesFilter = null;
    @SerializedName("propertyIds")
    @Expose
    public List<String> propertyIds = null;

    public List<String> getPolicyFilter() {
        return policyFilter;
    }

    public void setPolicyFilter(List<String> policyFilter) {
        this.policyFilter = policyFilter;
    }

    @SerializedName("policyFilter")
    @Expose
    public List<String> policyFilter = null;
    @SerializedName("showInvisibleProperties")
    @Expose
    public Boolean showInvisibleProperties;


    public Boolean getShowAllProperties() {
        return showAllProperties;
    }

    public void setShowAllProperties(Boolean showAllProperties) {
        this.showAllProperties = showAllProperties;
    }

    @SerializedName("showAllProperties")
    @Expose

    public Boolean showAllProperties;

    @SerializedName("sortOrder")
    @Expose
    public String sortOrder;
    @SerializedName("requestType")
    @Expose
    public String requestType;
    @SerializedName("responseType")
    @Expose
    public String responseType;
    @SerializedName("deviceType")
    @Expose
    public String deviceType;
    @SerializedName("pageType")
    @Expose
    public String pageType;
    @SerializedName("sessionId")
    @Expose
    public String sessionId;
    @SerializedName("dateLess")
    @Expose
    public Boolean dateLess;
    @SerializedName("mealPlan")
    @Expose
    public String mealPlan;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @SerializedName("requestId")
    @Expose

    public String requestId;

    public String getLatLong() {
        return latLong;
    }

    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }

    public List<Integer> getLocalityIds() {
        return localityIds;
    }

    public void setLocalityIds(List<Integer> localityIds) {
        this.localityIds = localityIds;
    }

    public Boolean getNearByRequired() {
        return nearByRequired;
    }

    public void setNearByRequired(Boolean nearByRequired) {
        this.nearByRequired = nearByRequired;
    }

    public Integer getPriceRangeMin() {
        return priceRangeMin;
    }

    public void setPriceRangeMin(Integer priceRangeMin) {
        this.priceRangeMin = priceRangeMin;
    }

    public Integer getPriceRangeMax() {
        return priceRangeMax;
    }

    public void setPriceRangeMax(Integer priceRangeMax) {
        this.priceRangeMax = priceRangeMax;
    }

    public Integer getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(Integer roomCount) {
        this.roomCount = roomCount;
    }

    public Integer getNoOfPax() {
        return noOfPax;
    }

    public void setNoOfPax(Integer noOfPax) {
        this.noOfPax = noOfPax;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public List<Integer> getDistanceBucket() {
        return distanceBucket;
    }

    public void setDistanceBucket(List<Integer> distanceBucket) {
        this.distanceBucket = distanceBucket;
    }

    public List<String> getPropertyIds() {
        return propertyIds;
    }

    public void setPropertyIds(List<String> propertyIds) {
        this.propertyIds = propertyIds;
    }

    public Boolean getShowInvisibleProperties() {
        return showInvisibleProperties;
    }

    public void setShowInvisibleProperties(Boolean showInvisibleProperties) {
        this.showInvisibleProperties = showInvisibleProperties;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Boolean getDateLess() {
        return dateLess;
    }

    public void setDateLess(Boolean dateLess) {
        this.dateLess = dateLess;
    }

    public String getMealPlan() {
        return mealPlan;
    }

    public void setMealPlan(String mealPlan) {
        this.mealPlan = mealPlan;
    }

    public Integer getMembershipLevel() {
        return membershipLevel;
    }

    public void setMembershipLevel(Integer membershipLevel) {
        this.membershipLevel = membershipLevel;
    }

    @SerializedName("membershipLevel")
    @Expose
    public Integer membershipLevel;


}
