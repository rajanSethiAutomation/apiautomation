package com.fabhotels.dto.request.Search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NearPlaceFilter {

    @SerializedName("metro")
    @Expose
    private Double metro;

    @SerializedName("airport")
    @Expose
    private Double airport;

    public Double getAirport() {
        return airport;
    }

    public void setAirport(Double airport) {
        this.airport = airport;
    }

    public Double getMetro() {
        return metro;
    }

    public void setMetro(Double metro) {
        this.metro = metro;
    }

}