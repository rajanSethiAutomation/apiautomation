
package com.fabhotels.dto.response.Search.searchPopularCities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AvgPoints {

    @SerializedName("propertyId")
    @Expose
    private Integer propertyId;
    @SerializedName("roomType")
    @Expose
    private String roomType;
    @SerializedName("roomTypeId")
    @Expose
    private String roomTypeId;
    @SerializedName("date")
    @Expose
    private Date date;
    @SerializedName("deviceType")
    @Expose
    private String deviceType;
    @SerializedName("singleOccupancyPoints")
    @Expose
    private Integer singleOccupancyPoints;
    @SerializedName("doubleOccupancyPoints")
    @Expose
    private Integer doubleOccupancyPoints;
    @SerializedName("tripleOccupancyPoints")
    @Expose
    private Integer tripleOccupancyPoints;

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }



}
