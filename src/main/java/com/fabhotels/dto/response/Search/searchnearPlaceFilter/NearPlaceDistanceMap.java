
package com.fabhotels.dto.response.Search.searchnearPlaceFilter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NearPlaceDistanceMap {

    @SerializedName("bus")
    @Expose
    private Bus bus;
    @SerializedName("metro")
    @Expose
    private Metro metro;
    @SerializedName("railway")
    @Expose
    private Railway railway;
    @SerializedName("airport")
    @Expose
    private Airport airport;

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public Metro getMetro() {
        return metro;
    }

    public void setMetro(Metro metro) {
        this.metro = metro;
    }

    public Railway getRailway() {
        return railway;
    }

    public void setRailway(Railway railway) {
        this.railway = railway;
    }

    public Airport getAirport() {
        return airport;
    }

    public void setAirport(Airport airport) {
        this.airport = airport;
    }

}
