
package com.fabhotels.dto.response.Search.searchPopularCities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PsrDto {

    @SerializedName("propertyId")
    @Expose
    private Integer propertyId;
    @SerializedName("totolRoomCount")
    @Expose
    private Integer totolRoomCount;
    @SerializedName("maxOccupancy")
    @Expose
    private Integer maxOccupancy;
    @SerializedName("soldOut")
    @Expose
    private Boolean soldOut;
    @SerializedName("avgPrice")
    @Expose
    private AvgPrice avgPrice;
    @SerializedName("avgPoints")
    @Expose
    private AvgPoints avgPoints;
    @SerializedName("minPrice")
    @Expose
    private MinPrice minPrice;
    @SerializedName("minPoints")
    @Expose
    private MinPoints minPoints;
    @SerializedName("metaDataDto")
    @Expose
    private MetaDataDto metaDataDto;
    @SerializedName("bookingDto")
    @Expose
    private BookingDto bookingDto;
    @SerializedName("roomTypeDtos")
    @Expose
    private List<RoomTypeDto> roomTypeDtos = null;
    @SerializedName("roomByRoomType")
    @Expose
    private RoomByRoomType roomByRoomType;
    @SerializedName("isNewProperty")
    @Expose
    private Boolean isNewProperty;
    @SerializedName("membershipDiscountGreaterThanZero")
    @Expose
    private Boolean membershipDiscountGreaterThanZero;
    @SerializedName("roomType")
    @Expose
    private String roomType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("roomTypeId")
    @Expose
    private Integer roomTypeId;
    @SerializedName("images")
    @Expose
    private List<Object> images = null;
    @SerializedName("usp")
    @Expose
    private List<Object> usp = null;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("isDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("isPublished")
    @Expose
    private Boolean isPublished;
    @SerializedName("retryCount")
    @Expose
    private Integer retryCount;
    @SerializedName("consumer")
    @Expose
    private String consumer;

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public Integer getTotolRoomCount() {
        return totolRoomCount;
    }

    public void setTotolRoomCount(Integer totolRoomCount) {
        this.totolRoomCount = totolRoomCount;
    }

    public Integer getMaxOccupancy() {
        return maxOccupancy;
    }

    public void setMaxOccupancy(Integer maxOccupancy) {
        this.maxOccupancy = maxOccupancy;
    }

    public Boolean getSoldOut() {
        return soldOut;
    }

    public void setSoldOut(Boolean soldOut) {
        this.soldOut = soldOut;
    }

    public AvgPrice getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(AvgPrice avgPrice) {
        this.avgPrice = avgPrice;
    }

    public AvgPoints getAvgPoints() {
        return avgPoints;
    }

    public void setAvgPoints(AvgPoints avgPoints) {
        this.avgPoints = avgPoints;
    }

    public MinPrice getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(MinPrice minPrice) {
        this.minPrice = minPrice;
    }

    public MinPoints getMinPoints() {
        return minPoints;
    }

    public void setMinPoints(MinPoints minPoints) {
        this.minPoints = minPoints;
    }

    public MetaDataDto getMetaDataDto() {
        return metaDataDto;
    }

    public void setMetaDataDto(MetaDataDto metaDataDto) {
        this.metaDataDto = metaDataDto;
    }

    public BookingDto getBookingDto() {
        return bookingDto;
    }

    public void setBookingDto(BookingDto bookingDto) {
        this.bookingDto = bookingDto;
    }

    public List<RoomTypeDto> getRoomTypeDtos() {
        return roomTypeDtos;
    }

    public void setRoomTypeDtos(List<RoomTypeDto> roomTypeDtos) {
        this.roomTypeDtos = roomTypeDtos;
    }

    public RoomByRoomType getRoomByRoomType() {
        return roomByRoomType;
    }

    public void setRoomByRoomType(RoomByRoomType roomByRoomType) {
        this.roomByRoomType = roomByRoomType;
    }

    public Boolean getIsNewProperty() {
        return isNewProperty;
    }

    public void setIsNewProperty(Boolean isNewProperty) {
        this.isNewProperty = isNewProperty;
    }

    public Boolean getMembershipDiscountGreaterThanZero() {
        return membershipDiscountGreaterThanZero;
    }

    public void setMembershipDiscountGreaterThanZero(Boolean membershipDiscountGreaterThanZero) {
        this.membershipDiscountGreaterThanZero = membershipDiscountGreaterThanZero;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRoomTypeId() {
        return roomTypeId;
    }

    public void setRoomTypeId(Integer roomTypeId) {
        this.roomTypeId = roomTypeId;
    }

    public List<Object> getImages() {
        return images;
    }

    public void setImages(List<Object> images) {
        this.images = images;
    }

    public List<Object> getUsp() {
        return usp;
    }

    public void setUsp(List<Object> usp) {
        this.usp = usp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsPublished() {
        return isPublished;
    }

    public void setIsPublished(Boolean isPublished) {
        this.isPublished = isPublished;
    }

    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    public String getConsumer() {
        return consumer;
    }

    public void setConsumer(String consumer) {
        this.consumer = consumer;
    }

}
