
package com.fabhotels.dto.response.Search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoomByRoomType {

    @SerializedName("ROOM_TYPE_A")
    @Expose
    private Integer rOOMTYPEA;

    public Integer getROOMTYPEA() {
        return rOOMTYPEA;
    }

    public void setROOMTYPEA(Integer rOOMTYPEA) {
        this.rOOMTYPEA = rOOMTYPEA;
    }

}
