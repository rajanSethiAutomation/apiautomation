
package com.fabhotels.dto.response.Search.searchPopularCities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MinPrice {

    @SerializedName("roomPriceSingle")
    @Expose
    private Double roomPriceSingle;
    @SerializedName("roomPriceSingleBase")
    @Expose
    private Double roomPriceSingleBase;
    @SerializedName("discountRoomPriceSingle")
    @Expose
    private Double discountRoomPriceSingle;
    @SerializedName("roomPriceSingleTax")
    @Expose
    private Double roomPriceSingleTax;
    @SerializedName("roomPriceDouble")
    @Expose
    private Double roomPriceDouble;
    @SerializedName("roomPriceDoubleBase")
    @Expose
    private Double roomPriceDoubleBase;
    @SerializedName("discountRoomPriceDouble")
    @Expose
    private Double discountRoomPriceDouble;
    @SerializedName("roomPriceDoubleTax")
    @Expose
    private Double roomPriceDoubleTax;
    @SerializedName("roomPriceTriple")
    @Expose
    private Double roomPriceTriple;
    @SerializedName("roomPriceTripleBase")
    @Expose
    private Double roomPriceTripleBase;
    @SerializedName("discountRoomPriceTriple")
    @Expose
    private Double discountRoomPriceTriple;
    @SerializedName("roomPriceTripleTax")
    @Expose
    private Double roomPriceTripleTax;

    public Double getRoomPriceSingle() {
        return roomPriceSingle;
    }

    public void setRoomPriceSingle(Double roomPriceSingle) {
        this.roomPriceSingle = roomPriceSingle;
    }

    public Double getRoomPriceSingleBase() {
        return roomPriceSingleBase;
    }

    public void setRoomPriceSingleBase(Double roomPriceSingleBase) {
        this.roomPriceSingleBase = roomPriceSingleBase;
    }

    public Double getDiscountRoomPriceSingle() {
        return discountRoomPriceSingle;
    }

    public void setDiscountRoomPriceSingle(Double discountRoomPriceSingle) {
        this.discountRoomPriceSingle = discountRoomPriceSingle;
    }

    public Double getRoomPriceSingleTax() {
        return roomPriceSingleTax;
    }

    public void setRoomPriceSingleTax(Double roomPriceSingleTax) {
        this.roomPriceSingleTax = roomPriceSingleTax;
    }

    public Double getRoomPriceDouble() {
        return roomPriceDouble;
    }

    public void setRoomPriceDouble(Double roomPriceDouble) {
        this.roomPriceDouble = roomPriceDouble;
    }

    public Double getRoomPriceDoubleBase() {
        return roomPriceDoubleBase;
    }

    public void setRoomPriceDoubleBase(Double roomPriceDoubleBase) {
        this.roomPriceDoubleBase = roomPriceDoubleBase;
    }

    public Double getDiscountRoomPriceDouble() {
        return discountRoomPriceDouble;
    }

    public void setDiscountRoomPriceDouble(Double discountRoomPriceDouble) {
        this.discountRoomPriceDouble = discountRoomPriceDouble;
    }

    public Double getRoomPriceDoubleTax() {
        return roomPriceDoubleTax;
    }

    public void setRoomPriceDoubleTax(Double roomPriceDoubleTax) {
        this.roomPriceDoubleTax = roomPriceDoubleTax;
    }

    public Double getRoomPriceTriple() {
        return roomPriceTriple;
    }

    public void setRoomPriceTriple(Double roomPriceTriple) {
        this.roomPriceTriple = roomPriceTriple;
    }

    public Double getRoomPriceTripleBase() {
        return roomPriceTripleBase;
    }

    public void setRoomPriceTripleBase(Double roomPriceTripleBase) {
        this.roomPriceTripleBase = roomPriceTripleBase;
    }

    public Double getDiscountRoomPriceTriple() {
        return discountRoomPriceTriple;
    }

    public void setDiscountRoomPriceTriple(Double discountRoomPriceTriple) {
        this.discountRoomPriceTriple = discountRoomPriceTriple;
    }

    public Double getRoomPriceTripleTax() {
        return roomPriceTripleTax;
    }

    public void setRoomPriceTripleTax(Double roomPriceTripleTax) {
        this.roomPriceTripleTax = roomPriceTripleTax;
    }

    @SerializedName("breakfastCost")
    @Expose
    private Integer breakfastCost;
    @SerializedName("retryCount")
    @Expose
    private Integer retryCount;
    @SerializedName("consumer")
    @Expose
    private String consumer;



    public Integer getBreakfastCost() {
        return breakfastCost;
    }

    public void setBreakfastCost(Integer breakfastCost) {
        this.breakfastCost = breakfastCost;
    }

    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    public String getConsumer() {
        return consumer;
    }

    public void setConsumer(String consumer) {
        this.consumer = consumer;
    }

}
