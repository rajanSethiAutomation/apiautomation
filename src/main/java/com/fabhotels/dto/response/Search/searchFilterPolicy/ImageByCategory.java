
package com.fabhotels.dto.response.Search.searchFilterPolicy;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageByCategory {

    @SerializedName("Main")
    @Expose
    private List<String> main = null;
    @SerializedName("Room Image")
    @Expose
    private List<String> roomImage = null;
    @SerializedName("Bathroom")
    @Expose
    private List<String> bathroom = null;
    @SerializedName("Restaurant")
    @Expose
    private List<String> restaurant = null;
    @SerializedName("Reception")
    @Expose
    private List<String> reception = null;
    @SerializedName("Faade")
    @Expose
    private List<String> faade = null;

    public List<String> getMain() {
        return main;
    }

    public void setMain(List<String> main) {
        this.main = main;
    }

    public List<String> getRoomImage() {
        return roomImage;
    }

    public void setRoomImage(List<String> roomImage) {
        this.roomImage = roomImage;
    }

    public List<String> getBathroom() {
        return bathroom;
    }

    public void setBathroom(List<String> bathroom) {
        this.bathroom = bathroom;
    }

    public List<String> getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(List<String> restaurant) {
        this.restaurant = restaurant;
    }

    public List<String> getReception() {
        return reception;
    }

    public void setReception(List<String> reception) {
        this.reception = reception;
    }

    public List<String> getFaade() {
        return faade;
    }

    public void setFaade(List<String> faade) {
        this.faade = faade;
    }

}
