
package com.fabhotels.dto.response.Search.searchPopularCities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NearPlace {

    @SerializedName("bus")
    @Expose
    private String bus;
    @SerializedName("busDistance")
    @Expose
    private String busDistance;
    @SerializedName("railway")
    @Expose
    private String railway;
    @SerializedName("railwayDistance")
    @Expose
    private String railwayDistance;
    @SerializedName("airport")
    @Expose
    private String airport;
    @SerializedName("airportDistance")
    @Expose
    private String airportDistance;
    @SerializedName("metro")
    @Expose
    private String metro;
    @SerializedName("metroDistance")
    @Expose
    private String metroDistance;
    @SerializedName("beach")
    @Expose
    private String beach;
    @SerializedName("beachDistance")
    @Expose
    private String beachDistance;

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public String getBusDistance() {
        return busDistance;
    }

    public void setBusDistance(String busDistance) {
        this.busDistance = busDistance;
    }

    public String getRailway() {
        return railway;
    }

    public void setRailway(String railway) {
        this.railway = railway;
    }

    public String getRailwayDistance() {
        return railwayDistance;
    }

    public void setRailwayDistance(String railwayDistance) {
        this.railwayDistance = railwayDistance;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getAirportDistance() {
        return airportDistance;
    }

    public void setAirportDistance(String airportDistance) {
        this.airportDistance = airportDistance;
    }

    public String getMetro() {
        return metro;
    }

    public void setMetro(String metro) {
        this.metro = metro;
    }

    public String getMetroDistance() {
        return metroDistance;
    }

    public void setMetroDistance(String metroDistance) {
        this.metroDistance = metroDistance;
    }

    public String getBeach() {
        return beach;
    }

    public void setBeach(String beach) {
        this.beach = beach;
    }

    public String getBeachDistance() {
        return beachDistance;
    }

    public void setBeachDistance(String beachDistance) {
        this.beachDistance = beachDistance;
    }

}
