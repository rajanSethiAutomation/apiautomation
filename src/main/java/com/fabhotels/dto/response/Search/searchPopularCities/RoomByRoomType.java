
package com.fabhotels.dto.response.Search.searchPopularCities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoomByRoomType {

    @SerializedName("ROOM_TYPE_A")
    @Expose
    private Integer rOOMTYPEA;
    @SerializedName("ROOM_TYPE_B")
    @Expose
    private Integer rOOMTYPEB;

    public Integer getROOMTYPEA() {
        return rOOMTYPEA;
    }

    public void setROOMTYPEA(Integer rOOMTYPEA) {
        this.rOOMTYPEA = rOOMTYPEA;
    }

    public Integer getROOMTYPEB() {
        return rOOMTYPEB;
    }

    public void setROOMTYPEB(Integer rOOMTYPEB) {
        this.rOOMTYPEB = rOOMTYPEB;
    }

}
