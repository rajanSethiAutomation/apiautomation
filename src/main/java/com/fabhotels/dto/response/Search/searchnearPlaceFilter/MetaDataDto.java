
package com.fabhotels.dto.response.Search.searchnearPlaceFilter;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MetaDataDto {

    @SerializedName("propertyId")
    @Expose
    private Integer propertyId;
    @SerializedName("liveDate")
    @Expose
    private String liveDate;
    @SerializedName("propertyName")
    @Expose
    private String propertyName;
    @SerializedName("otaPropertyName")
    @Expose
    private String otaPropertyName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("stateId")
    @Expose
    private Integer stateId;
    @SerializedName("stateName")
    @Expose
    private String stateName;
    @SerializedName("cityId")
    @Expose
    private Integer cityId;
    @SerializedName("cityName")
    @Expose
    private String cityName;
    @SerializedName("displayOrder")
    @Expose
    private Integer displayOrder;
    @SerializedName("imageUrls")
    @Expose
    private List<String> imageUrls = null;
    @SerializedName("active")
    @Expose
    private Boolean active;
    @SerializedName("propertyFabManaged")
    @Expose
    private Boolean propertyFabManaged;
    @SerializedName("propertyManaged")
    @Expose
    private Boolean propertyManaged;
    @SerializedName("propertyPriority")
    @Expose
    private Boolean propertyPriority;
    @SerializedName("visible")
    @Expose
    private Boolean visible;
    @SerializedName("landingUrl")
    @Expose
    private String landingUrl;
    @SerializedName("localityId")
    @Expose
    private List<Integer> localityId = null;
    @SerializedName("localityName")
    @Expose
    private List<String> localityName = null;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("metro")
    @Expose
    private String metro;
    @SerializedName("nearText")
    @Expose
    private String nearText;
    @SerializedName("railwayStation")
    @Expose
    private String railwayStation;
    @SerializedName("airport")
    @Expose
    private String airport;
    @SerializedName("beach")
    @Expose
    private String beach;
    @SerializedName("busStand")
    @Expose
    private String busStand;
    @SerializedName("tripadvisorRating")
    @Expose
    private Double tripadvisorRating;
    @SerializedName("review")
    @Expose
    private Integer review;
    @SerializedName("googleMapUrl")
    @Expose
    private String googleMapUrl;
    @SerializedName("propertyTax")
    @Expose
    private Double propertyTax;
    @SerializedName("quesAndAns")
    @Expose
    private QuesAndAns quesAndAns;
    @SerializedName("nearPlace")
    @Expose
    private NearPlace nearPlace;
    @SerializedName("nearPlaceDistance")
    @Expose
    private List<NearPlaceDistance> nearPlaceDistance = null;
    @SerializedName("nearPlaceDistanceMap")
    @Expose
    private NearPlaceDistanceMap nearPlaceDistanceMap;
    @SerializedName("amenities")
    @Expose
    private List<Integer> amenities = null;
    @SerializedName("pincode")
    @Expose
    private Integer pincode;
    @SerializedName("imageByCategory")
    @Expose
    private ImageByCategory imageByCategory;
    @SerializedName("hotelOfficers")
    @Expose
    private HotelOfficers hotelOfficers;
    @SerializedName("propertyLocality")
    @Expose
    private String propertyLocality;
    @SerializedName("featured")
    @Expose
    private Boolean featured;
    @SerializedName("metaTitle")
    @Expose
    private String metaTitle;
    @SerializedName("metaKeywords")
    @Expose
    private String metaKeywords;
    @SerializedName("metaDescription")
    @Expose
    private String metaDescription;
    @SerializedName("propertyFabRating")
    @Expose
    private Double propertyFabRating;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("isDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("isPublished")
    @Expose
    private Boolean isPublished;
    @SerializedName("retryCount")
    @Expose
    private Integer retryCount;
    @SerializedName("consumer")
    @Expose
    private String consumer;
    @SerializedName("policies")
    @Expose
    private List<Policy> policies = null;

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public String getLiveDate() {
        return liveDate;
    }

    public void setLiveDate(String liveDate) {
        this.liveDate = liveDate;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getOtaPropertyName() {
        return otaPropertyName;
    }

    public void setOtaPropertyName(String otaPropertyName) {
        this.otaPropertyName = otaPropertyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getPropertyFabManaged() {
        return propertyFabManaged;
    }

    public void setPropertyFabManaged(Boolean propertyFabManaged) {
        this.propertyFabManaged = propertyFabManaged;
    }

    public Boolean getPropertyManaged() {
        return propertyManaged;
    }

    public void setPropertyManaged(Boolean propertyManaged) {
        this.propertyManaged = propertyManaged;
    }

    public Boolean getPropertyPriority() {
        return propertyPriority;
    }

    public void setPropertyPriority(Boolean propertyPriority) {
        this.propertyPriority = propertyPriority;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String getLandingUrl() {
        return landingUrl;
    }

    public void setLandingUrl(String landingUrl) {
        this.landingUrl = landingUrl;
    }

    public List<Integer> getLocalityId() {
        return localityId;
    }

    public void setLocalityId(List<Integer> localityId) {
        this.localityId = localityId;
    }

    public List<String> getLocalityName() {
        return localityName;
    }

    public void setLocalityName(List<String> localityName) {
        this.localityName = localityName;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getMetro() {
        return metro;
    }

    public void setMetro(String metro) {
        this.metro = metro;
    }

    public String getNearText() {
        return nearText;
    }

    public void setNearText(String nearText) {
        this.nearText = nearText;
    }

    public String getRailwayStation() {
        return railwayStation;
    }

    public void setRailwayStation(String railwayStation) {
        this.railwayStation = railwayStation;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getBeach() {
        return beach;
    }

    public void setBeach(String beach) {
        this.beach = beach;
    }

    public String getBusStand() {
        return busStand;
    }

    public void setBusStand(String busStand) {
        this.busStand = busStand;
    }

    public Double getTripadvisorRating() {
        return tripadvisorRating;
    }

    public void setTripadvisorRating(Double tripadvisorRating) {
        this.tripadvisorRating = tripadvisorRating;
    }

    public Integer getReview() {
        return review;
    }

    public void setReview(Integer review) {
        this.review = review;
    }

    public String getGoogleMapUrl() {
        return googleMapUrl;
    }

    public void setGoogleMapUrl(String googleMapUrl) {
        this.googleMapUrl = googleMapUrl;
    }

    public Double getPropertyTax() {
        return propertyTax;
    }

    public void setPropertyTax(Double propertyTax) {
        this.propertyTax = propertyTax;
    }

    public QuesAndAns getQuesAndAns() {
        return quesAndAns;
    }

    public void setQuesAndAns(QuesAndAns quesAndAns) {
        this.quesAndAns = quesAndAns;
    }

    public NearPlace getNearPlace() {
        return nearPlace;
    }

    public void setNearPlace(NearPlace nearPlace) {
        this.nearPlace = nearPlace;
    }

    public List<NearPlaceDistance> getNearPlaceDistance() {
        return nearPlaceDistance;
    }

    public void setNearPlaceDistance(List<NearPlaceDistance> nearPlaceDistance) {
        this.nearPlaceDistance = nearPlaceDistance;
    }

    public NearPlaceDistanceMap getNearPlaceDistanceMap() {
        return nearPlaceDistanceMap;
    }

    public void setNearPlaceDistanceMap(NearPlaceDistanceMap nearPlaceDistanceMap) {
        this.nearPlaceDistanceMap = nearPlaceDistanceMap;
    }

    public List<Integer> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<Integer> amenities) {
        this.amenities = amenities;
    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

    public ImageByCategory getImageByCategory() {
        return imageByCategory;
    }

    public void setImageByCategory(ImageByCategory imageByCategory) {
        this.imageByCategory = imageByCategory;
    }

    public HotelOfficers getHotelOfficers() {
        return hotelOfficers;
    }

    public void setHotelOfficers(HotelOfficers hotelOfficers) {
        this.hotelOfficers = hotelOfficers;
    }

    public String getPropertyLocality() {
        return propertyLocality;
    }

    public void setPropertyLocality(String propertyLocality) {
        this.propertyLocality = propertyLocality;
    }

    public Boolean getFeatured() {
        return featured;
    }

    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }

    public String getMetaTitle() {
        return metaTitle;
    }

    public void setMetaTitle(String metaTitle) {
        this.metaTitle = metaTitle;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public Double getPropertyFabRating() {
        return propertyFabRating;
    }

    public void setPropertyFabRating(Double propertyFabRating) {
        this.propertyFabRating = propertyFabRating;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsPublished() {
        return isPublished;
    }

    public void setIsPublished(Boolean isPublished) {
        this.isPublished = isPublished;
    }

    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    public String getConsumer() {
        return consumer;
    }

    public void setConsumer(String consumer) {
        this.consumer = consumer;
    }

    public List<Policy> getPolicies() {
        return policies;
    }

    public void setPolicies(List<Policy> policies) {
        this.policies = policies;
    }

}
