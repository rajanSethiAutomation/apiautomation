
package com.fabhotels.dto.response.Search.searchnearPlaceFilter;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchPropertiesnearPlaceFilterPolicy {

    @SerializedName("totalDto")
    @Expose
    private Integer totalDto;
    @SerializedName("minPrice")
    @Expose
    private Double minPrice;
    @SerializedName("maxPrice")
    @Expose
    private Double maxPrice;
    @SerializedName("transportDistance")
    @Expose
    private List<TransportDistance> transportDistance = null;
    @SerializedName("amenities")
    @Expose
    private List<Integer> amenities = null;
    @SerializedName("maxOccupancy")
    @Expose
    private Integer maxOccupancy;
    @SerializedName("policiesCode")
    @Expose
    private List<String> policiesCode = null;
    @SerializedName("propertyIds")
    @Expose
    private List<Integer> propertyIds = null;
    @SerializedName("psrDtos")
    @Expose
    private List<PsrDto> psrDtos = null;
    @SerializedName("memberShipDiscountApplied")
    @Expose
    private Boolean memberShipDiscountApplied;

    public Integer getTotalDto() {
        return totalDto;
    }

    public void setTotalDto(Integer totalDto) {
        this.totalDto = totalDto;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public List<TransportDistance> getTransportDistance() {
        return transportDistance;
    }

    public void setTransportDistance(List<TransportDistance> transportDistance) {
        this.transportDistance = transportDistance;
    }

    public List<Integer> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<Integer> amenities) {
        this.amenities = amenities;
    }

    public Integer getMaxOccupancy() {
        return maxOccupancy;
    }

    public void setMaxOccupancy(Integer maxOccupancy) {
        this.maxOccupancy = maxOccupancy;
    }

    public List<String> getPoliciesCode() {
        return policiesCode;
    }

    public void setPoliciesCode(List<String> policiesCode) {
        this.policiesCode = policiesCode;
    }

    public List<Integer> getPropertyIds() {
        return propertyIds;
    }

    public void setPropertyIds(List<Integer> propertyIds) {
        this.propertyIds = propertyIds;
    }

    public List<PsrDto> getPsrDtos() {
        return psrDtos;
    }

    public void setPsrDtos(List<PsrDto> psrDtos) {
        this.psrDtos = psrDtos;
    }

    public Boolean getMemberShipDiscountApplied() {
        return memberShipDiscountApplied;
    }

    public void setMemberShipDiscountApplied(Boolean memberShipDiscountApplied) {
        this.memberShipDiscountApplied = memberShipDiscountApplied;
    }

}
