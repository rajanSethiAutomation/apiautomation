
package com.fabhotels.dto.response.Search.searchFilterPolicy;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PsrDto {

    @SerializedName("propertyId")
    @Expose
    private Integer propertyId;
    @SerializedName("totolRoomCount")
    @Expose
    private Integer totolRoomCount;
    @SerializedName("maxOccupancy")
    @Expose
    private Integer maxOccupancy;
    @SerializedName("soldOut")
    @Expose
    private Boolean soldOut;
    @SerializedName("avgPrice")
    @Expose
    private AvgPrice avgPrice;
    @SerializedName("avgPoints")
    @Expose
    private AvgPoints avgPoints;
    @SerializedName("minPrice")
    @Expose
    private MinPrice minPrice;
    @SerializedName("minPoints")
    @Expose
    private MinPoints minPoints;
    @SerializedName("metaDataDto")
    @Expose
    private MetaDataDto metaDataDto;
    @SerializedName("bookingDto")
    @Expose
    private BookingDto bookingDto;
    @SerializedName("roomTypeDtos")
    @Expose
    private List<RoomTypeDto> roomTypeDtos = null;
    @SerializedName("roomByRoomType")
    @Expose
    private RoomByRoomType roomByRoomType;
    @SerializedName("isNewProperty")
    @Expose
    private Boolean isNewProperty;
    @SerializedName("membershipDiscountGreaterThanZero")
    @Expose
    private Boolean membershipDiscountGreaterThanZero;

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public Integer getTotolRoomCount() {
        return totolRoomCount;
    }

    public void setTotolRoomCount(Integer totolRoomCount) {
        this.totolRoomCount = totolRoomCount;
    }

    public Integer getMaxOccupancy() {
        return maxOccupancy;
    }

    public void setMaxOccupancy(Integer maxOccupancy) {
        this.maxOccupancy = maxOccupancy;
    }

    public Boolean getSoldOut() {
        return soldOut;
    }

    public void setSoldOut(Boolean soldOut) {
        this.soldOut = soldOut;
    }

    public AvgPrice getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(AvgPrice avgPrice) {
        this.avgPrice = avgPrice;
    }

    public AvgPoints getAvgPoints() {
        return avgPoints;
    }

    public void setAvgPoints(AvgPoints avgPoints) {
        this.avgPoints = avgPoints;
    }

    public MinPrice getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(MinPrice minPrice) {
        this.minPrice = minPrice;
    }

    public MinPoints getMinPoints() {
        return minPoints;
    }

    public void setMinPoints(MinPoints minPoints) {
        this.minPoints = minPoints;
    }

    public MetaDataDto getMetaDataDto() {
        return metaDataDto;
    }

    public void setMetaDataDto(MetaDataDto metaDataDto) {
        this.metaDataDto = metaDataDto;
    }

    public BookingDto getBookingDto() {
        return bookingDto;
    }

    public void setBookingDto(BookingDto bookingDto) {
        this.bookingDto = bookingDto;
    }

    public List<RoomTypeDto> getRoomTypeDtos() {
        return roomTypeDtos;
    }

    public void setRoomTypeDtos(List<RoomTypeDto> roomTypeDtos) {
        this.roomTypeDtos = roomTypeDtos;
    }

    public RoomByRoomType getRoomByRoomType() {
        return roomByRoomType;
    }

    public void setRoomByRoomType(RoomByRoomType roomByRoomType) {
        this.roomByRoomType = roomByRoomType;
    }

    public Boolean getIsNewProperty() {
        return isNewProperty;
    }

    public void setIsNewProperty(Boolean isNewProperty) {
        this.isNewProperty = isNewProperty;
    }

    public Boolean getMembershipDiscountGreaterThanZero() {
        return membershipDiscountGreaterThanZero;
    }

    public void setMembershipDiscountGreaterThanZero(Boolean membershipDiscountGreaterThanZero) {
        this.membershipDiscountGreaterThanZero = membershipDiscountGreaterThanZero;
    }

}
