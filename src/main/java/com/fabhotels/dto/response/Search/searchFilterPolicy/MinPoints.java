
package com.fabhotels.dto.response.Search.searchFilterPolicy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MinPoints {

    @SerializedName("propertyId")
    @Expose
    private Integer propertyId;
    @SerializedName("roomType")
    @Expose
    private String roomType;
    @SerializedName("roomTypeId")
    @Expose
    private String roomTypeId;
    @SerializedName("date")
    @Expose
    private Date_ date;
    @SerializedName("deviceType")
    @Expose
    private String deviceType;
    @SerializedName("singleOccupancyPoints")
    @Expose
    private Double singleOccupancyPoints;
    @SerializedName("doubleOccupancyPoints")
    @Expose
    private Double doubleOccupancyPoints;
    @SerializedName("tripleOccupancyPoints")
    @Expose
    private Double tripleOccupancyPoints;

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getRoomTypeId() {
        return roomTypeId;
    }

    public void setRoomTypeId(String roomTypeId) {
        this.roomTypeId = roomTypeId;
    }

    public Date_ getDate() {
        return date;
    }

    public void setDate(Date_ date) {
        this.date = date;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public Double getSingleOccupancyPoints() {
        return singleOccupancyPoints;
    }

    public void setSingleOccupancyPoints(Double singleOccupancyPoints) {
        this.singleOccupancyPoints = singleOccupancyPoints;
    }

    public Double getDoubleOccupancyPoints() {
        return doubleOccupancyPoints;
    }

    public void setDoubleOccupancyPoints(Double doubleOccupancyPoints) {
        this.doubleOccupancyPoints = doubleOccupancyPoints;
    }

    public Double getTripleOccupancyPoints() {
        return tripleOccupancyPoints;
    }

    public void setTripleOccupancyPoints(Double tripleOccupancyPoints) {
        this.tripleOccupancyPoints = tripleOccupancyPoints;
    }

}
