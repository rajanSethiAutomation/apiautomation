
package com.fabhotels.dto.response.Search.searchPopularCities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageByCategory {

    @SerializedName("Main")
    @Expose
    private List<String> main = null;
    @SerializedName("Deluxe Room")
    @Expose
    private List<String> deluxeRoom = null;
    @SerializedName("Room")
    @Expose
    private List<String> room = null;
    @SerializedName("Sitting Area")
    @Expose
    private List<String> sittingArea = null;
    @SerializedName("Bathroom")
    @Expose
    private List<String> bathroom = null;
    @SerializedName("Lobby")
    @Expose
    private List<String> lobby = null;
    @SerializedName("Lift")
    @Expose
    private List<String> lift = null;
    @SerializedName("TV Area")
    @Expose
    private List<String> tVArea = null;

    public List<String> getMain() {
        return main;
    }

    public void setMain(List<String> main) {
        this.main = main;
    }

    public List<String> getDeluxeRoom() {
        return deluxeRoom;
    }

    public void setDeluxeRoom(List<String> deluxeRoom) {
        this.deluxeRoom = deluxeRoom;
    }

    public List<String> getRoom() {
        return room;
    }

    public void setRoom(List<String> room) {
        this.room = room;
    }

    public List<String> getSittingArea() {
        return sittingArea;
    }

    public void setSittingArea(List<String> sittingArea) {
        this.sittingArea = sittingArea;
    }

    public List<String> getBathroom() {
        return bathroom;
    }

    public void setBathroom(List<String> bathroom) {
        this.bathroom = bathroom;
    }

    public List<String> getLobby() {
        return lobby;
    }

    public void setLobby(List<String> lobby) {
        this.lobby = lobby;
    }

    public List<String> getLift() {
        return lift;
    }

    public void setLift(List<String> lift) {
        this.lift = lift;
    }

    public List<String> getTVArea() {
        return tVArea;
    }

    public void setTVArea(List<String> tVArea) {
        this.tVArea = tVArea;
    }

}
