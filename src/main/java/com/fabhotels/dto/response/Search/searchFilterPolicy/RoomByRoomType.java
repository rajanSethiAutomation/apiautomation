
package com.fabhotels.dto.response.Search.searchFilterPolicy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoomByRoomType {

    @SerializedName("ROOM_TYPE_B")
    @Expose
    private Integer rOOMTYPEB;
    @SerializedName("ROOM_TYPE_C")
    @Expose
    private Integer rOOMTYPEC;
    @SerializedName("ROOM_TYPE_A")
    @Expose
    private Integer rOOMTYPEA;

    public Integer getROOMTYPEB() {
        return rOOMTYPEB;
    }

    public void setROOMTYPEB(Integer rOOMTYPEB) {
        this.rOOMTYPEB = rOOMTYPEB;
    }

    public Integer getROOMTYPEC() {
        return rOOMTYPEC;
    }

    public void setROOMTYPEC(Integer rOOMTYPEC) {
        this.rOOMTYPEC = rOOMTYPEC;
    }

    public Integer getROOMTYPEA() {
        return rOOMTYPEA;
    }

    public void setROOMTYPEA(Integer rOOMTYPEA) {
        this.rOOMTYPEA = rOOMTYPEA;
    }

}
