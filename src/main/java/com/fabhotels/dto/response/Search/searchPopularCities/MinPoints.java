
package com.fabhotels.dto.response.Search.searchPopularCities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MinPoints {

    @SerializedName("propertyId")
    @Expose
    private Integer propertyId;
    @SerializedName("roomType")
    @Expose
    private String roomType;
    @SerializedName("roomTypeId")
    @Expose
    private String roomTypeId;
    @SerializedName("date")
    @Expose
    private Date_ date;
    @SerializedName("deviceType")
    @Expose
    private String deviceType;
    @SerializedName("singleOccupancyPoints")
    @Expose
    private Integer singleOccupancyPoints;
    @SerializedName("doubleOccupancyPoints")
    @Expose
    private Integer doubleOccupancyPoints;
    @SerializedName("tripleOccupancyPoints")
    @Expose
    private Integer tripleOccupancyPoints;

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getRoomTypeId() {
        return roomTypeId;
    }

    public void setRoomTypeId(String roomTypeId) {
        this.roomTypeId = roomTypeId;
    }

    public Date_ getDate() {
        return date;
    }

    public void setDate(Date_ date) {
        this.date = date;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public Integer getSingleOccupancyPoints() {
        return singleOccupancyPoints;
    }

    public void setSingleOccupancyPoints(Integer singleOccupancyPoints) {
        this.singleOccupancyPoints = singleOccupancyPoints;
    }

    public Integer getDoubleOccupancyPoints() {
        return doubleOccupancyPoints;
    }

    public void setDoubleOccupancyPoints(Integer doubleOccupancyPoints) {
        this.doubleOccupancyPoints = doubleOccupancyPoints;
    }

    public Integer getTripleOccupancyPoints() {
        return tripleOccupancyPoints;
    }

    public void setTripleOccupancyPoints(Integer tripleOccupancyPoints) {
        this.tripleOccupancyPoints = tripleOccupancyPoints;
    }

}
