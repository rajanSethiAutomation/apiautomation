
package com.fabhotels.dto.response.Search;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchPropertiesProperties {

    @SerializedName("totalDto")
    @Expose
    private Integer totalDto;
    @SerializedName("minPrice")
    @Expose
    private Double minPrice;
    @SerializedName("maxPrice")
    @Expose
    private Double maxPrice;
    @SerializedName("transportDistance")
    @Expose
    private List<Object> transportDistance = null;
    @SerializedName("amenities")
    @Expose
    private List<Integer> amenities = null;
    @SerializedName("maxOccupancy")
    @Expose
    private Integer maxOccupancy;
    @SerializedName("policiesCode")
    @Expose
    private List<Object> policiesCode = null;
    @SerializedName("propertyIds")
    @Expose
    private List<Integer> propertyIds = null;
    @SerializedName("psrDtos")
    @Expose
    private List<PsrDto> psrDtos;
    @SerializedName("memberShipDiscountApplied")
    @Expose
    private Boolean memberShipDiscountApplied;

    public Integer getTotalDto() {
        return totalDto;
    }

    public void setTotalDto(Integer totalDto) {
        this.totalDto = totalDto;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public List<Object> getTransportDistance() {
        return transportDistance;
    }

    public void setTransportDistance(List<Object> transportDistance) {
        this.transportDistance = transportDistance;
    }

    public List<Integer> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<Integer> amenities) {
        this.amenities = amenities;
    }

    public Integer getMaxOccupancy() {
        return maxOccupancy;
    }

    public void setMaxOccupancy(Integer maxOccupancy) {
        this.maxOccupancy = maxOccupancy;
    }

    public List<Object> getPoliciesCode() {
        return policiesCode;
    }

    public void setPoliciesCode(List<Object> policiesCode) {
        this.policiesCode = policiesCode;
    }

    public List<Integer> getPropertyIds() {
        return propertyIds;
    }

    public void setPropertyIds(List<Integer> propertyIds) {
        this.propertyIds = propertyIds;
    }

    public List<PsrDto> getPsrDtos() {
        return psrDtos;
    }

    public void setPsrDtos(List<PsrDto> psrDtos) {
        this.psrDtos = psrDtos;
    }

    public Boolean getMemberShipDiscountApplied() {
        return memberShipDiscountApplied;
    }

    public void setMemberShipDiscountApplied(Boolean memberShipDiscountApplied) {
        this.memberShipDiscountApplied = memberShipDiscountApplied;
    }

}
