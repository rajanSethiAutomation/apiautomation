
package com.fabhotels.dto.response.Search.searchnearPlaceFilter;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageByCategory {

    @SerializedName("Main")
    @Expose
    private List<String> main = null;
    @SerializedName("Deluxe Room")
    @Expose
    private List<String> deluxeRoom = null;
    @SerializedName("Bedroom")
    @Expose
    private List<String> bedroom = null;
    @SerializedName("Bedroom Sitting Area")
    @Expose
    private List<String> bedroomSittingArea = null;
    @SerializedName("Sitting Area")
    @Expose
    private List<String> sittingArea = null;
    @SerializedName("Bathroom")
    @Expose
    private List<String> bathroom = null;
    @SerializedName("Corridor")
    @Expose
    private List<String> corridor = null;
    @SerializedName("Interior")
    @Expose
    private List<String> interior = null;
    @SerializedName("Double Bedroom")
    @Expose
    private List<String> doubleBedroom = null;
    @SerializedName("Reception")
    @Expose
    private List<String> reception = null;
    @SerializedName("Facade")
    @Expose
    private List<String> facade = null;
    @SerializedName("Room Image")
    @Expose
    private List<String> roomImage = null;
    @SerializedName("Restaurant")
    @Expose
    private List<String> restaurant = null;
    @SerializedName("Faade")
    @Expose
    private List<String> faade = null;

    public List<String> getMain() {
        return main;
    }

    public void setMain(List<String> main) {
        this.main = main;
    }

    public List<String> getDeluxeRoom() {
        return deluxeRoom;
    }

    public void setDeluxeRoom(List<String> deluxeRoom) {
        this.deluxeRoom = deluxeRoom;
    }

    public List<String> getBedroom() {
        return bedroom;
    }

    public void setBedroom(List<String> bedroom) {
        this.bedroom = bedroom;
    }

    public List<String> getBedroomSittingArea() {
        return bedroomSittingArea;
    }

    public void setBedroomSittingArea(List<String> bedroomSittingArea) {
        this.bedroomSittingArea = bedroomSittingArea;
    }

    public List<String> getSittingArea() {
        return sittingArea;
    }

    public void setSittingArea(List<String> sittingArea) {
        this.sittingArea = sittingArea;
    }

    public List<String> getBathroom() {
        return bathroom;
    }

    public void setBathroom(List<String> bathroom) {
        this.bathroom = bathroom;
    }

    public List<String> getCorridor() {
        return corridor;
    }

    public void setCorridor(List<String> corridor) {
        this.corridor = corridor;
    }

    public List<String> getInterior() {
        return interior;
    }

    public void setInterior(List<String> interior) {
        this.interior = interior;
    }

    public List<String> getDoubleBedroom() {
        return doubleBedroom;
    }

    public void setDoubleBedroom(List<String> doubleBedroom) {
        this.doubleBedroom = doubleBedroom;
    }

    public List<String> getReception() {
        return reception;
    }

    public void setReception(List<String> reception) {
        this.reception = reception;
    }

    public List<String> getFacade() {
        return facade;
    }

    public void setFacade(List<String> facade) {
        this.facade = facade;
    }

    public List<String> getRoomImage() {
        return roomImage;
    }

    public void setRoomImage(List<String> roomImage) {
        this.roomImage = roomImage;
    }

    public List<String> getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(List<String> restaurant) {
        this.restaurant = restaurant;
    }

    public List<String> getFaade() {
        return faade;
    }

    public void setFaade(List<String> faade) {
        this.faade = faade;
    }

}
