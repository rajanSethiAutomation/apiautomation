
package com.fabhotels.dto.response.Search;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageByCategory {

    @SerializedName("Main")
    @Expose
    private List<String> main = null;
    @SerializedName("Reception")
    @Expose
    private List<String> reception = null;

    public List<String> getMain() {
        return main;
    }

    public void setMain(List<String> main) {
        this.main = main;
    }

    public List<String> getReception() {
        return reception;
    }

    public void setReception(List<String> reception) {
        this.reception = reception;
    }

}
