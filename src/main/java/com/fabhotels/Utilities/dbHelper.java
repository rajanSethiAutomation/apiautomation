package com.fabhotels.Utilities;

import com.fabhotels.common.helper.common.ApiHelper;

import com.fabhotels.common.helper.report.FileHelper;
import org.testng.Reporter;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class dbHelper {

    static final String CONFIGURATION_FILE_PATH = "config/envConfig.properties";

    public dbHelper() {
    }

    public static Connection createDbConnection(String environment, String sid) {
        Connection con = null;
        String dbServerIp = ApiHelper.getValueOfProperty("config/envConfig.properties", environment + "_IP_MySql");
        String userName = ApiHelper.getValueOfProperty("config/envConfig.properties", environment + "_USER_MySql");
        String password = ApiHelper.getValueOfProperty("config/envConfig.properties", environment + "_PASSWORD_MySql");

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            String connectionUrl = "jdbc:mysql://localhost/"+sid;
            //String connectionString = "jdbc:oracle:thin:@" + dbServerIp + ":1521:" + sid;
            con = DriverManager.getConnection(connectionUrl, userName, password);
        } catch (Exception var7) {
            Reporter.log(var7.getLocalizedMessage());
        }

        return con;
    }

    public static List<Map<String, String>> fetchDatafromDb(Connection connection, String query) {
        new ArrayList();
        ResultSet result = executeQuery(connection, query);
        List<Map<String, String>> resultData = convertResultToMap(result);
        return resultData;
    }

    private static ResultSet executeQuery(Connection connection, String query) {
        ResultSet result = null;

        try {
            if (connection != null) {
                Statement stmt = connection.createStatement();
                result = stmt.executeQuery(query);

                System.out.println(result);
            }
        } catch (SQLException var4) {
            var4.printStackTrace();
        }

        return result;
    }

    private static void closeConnection(Connection conn) {
        try {
            conn.close();
        } catch (SQLException var2) {
            Reporter.log("Error while closing connection");
        }

    }

    private static List<Map<String, String>> convertResultToMap(ResultSet resultSet) {
        ArrayList queryData = new ArrayList();

        try {
            int columnLength = resultSet.getMetaData().getColumnCount();

            while(resultSet.next()) {
                HashMap<String, String> rowData = new HashMap();

                for(int colIterator = 1; colIterator <= columnLength; ++colIterator) {
                    String colName = resultSet.getMetaData().getColumnLabel(colIterator);
                    String colValue = resultSet.getString(colName);
                    rowData.put(colName, colValue);
                }

                queryData.add(rowData);
            }
        } catch (SQLException var7) {
            Reporter.log(var7.getMessage());
        }

        return queryData;
    }

    public static void fetchDBDataToFile(Connection connection, String query, String fileName) {
        try {
            FileHelper fileHelper = new FileHelper();
            ResultSet result = executeQuery(connection, query);
            int columnLength = result.getMetaData().getColumnCount();

            while(result.next()) {
                String rowData = "";
                StringBuilder sb = new StringBuilder();

                for(int colIterator = 1; colIterator <= columnLength; ++colIterator) {
                    String value = result.getString(colIterator);
                    sb.append(value).append("---");
                }

                sb.setLength(sb.length() - 3);
                sb.append(System.getProperty("line.separator"));
                rowData = sb.toString();
                fileHelper.writeToFile(rowData, fileName, true);
            }
        } catch (SQLException var10) {
            Reporter.log(var10.getMessage());
        }

        closeConnection(connection);
    }
}

