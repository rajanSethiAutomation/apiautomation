package com.fabhotels.Utilities;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class dbTestData   {

    public dbTestData(){
    }

    @DataProvider(
            name = "dbDataProvider"
    )
    protected Iterator<Object[]> getDataFromDB(Method method) {
        Connection connection;
        List<String> queryData = new ArrayList();
        new ArrayList();
        List<Object[]> testData = new ArrayList();
        Test tstData = (Test)method.getAnnotation(Test.class);
        if (tstData != null && tstData.groups() != null) {
            String[] var6 = tstData.groups();
            int var7 = var6.length;

            for(int var8 = 0; var8 < var7; ++var8) {
                String queryGroup = var6[var8];
                queryData.add(queryGroup);
            }
        }

        String query = (String)queryData.get(0);
        String dbEnvironment = (String)queryData.get(1);
        String sid = (String)queryData.get(2);
        connection = dbHelper.createDbConnection(dbEnvironment, sid);
        List<Map<String, String>> queryResultData = dbHelper.fetchDatafromDb(connection, query);
        Iterator finalData = queryResultData.iterator();

        while(finalData.hasNext()) {
            Map<String, String> queryResult = (Map)finalData.next();
            testData.add(new Object[]{queryResult});
        }

        finalData = testData.iterator();
        return finalData;
    }


    @DataProvider(
            name = "ExcelDataProvider"
    )
    public static Object[][] getDataFromDataProvider(Method method) {
        String packageName = method.getDeclaringClass().getPackage().getName();
        packageName = packageName.split("RestAssured.")[1];
        packageName = packageName.replace(".", "/");
        String pathName = "src" + File.separator + "test" + File.separator + "resources" + File.separator + "test_data" + File.separator + packageName + File.separator  + "RequestKey.xlsx";
        Object[][] testDataObject = ExcelHelper.getDataFromExcel(pathName);
        return testDataObject;
    }

}
