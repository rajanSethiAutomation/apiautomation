package com.fabhotels.Utilities;

import io.restassured.response.Response;
import org.testng.asserts.SoftAssert;

public class ValidationHelper {


    public static void ValidateTwoResponse(Response preproduction_Response, Response production_Response) {


        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(preproduction_Response.then().extract().asString(),production_Response.then().extract().asString(),"Different Responses");
        softAssert.assertAll();


    }
}
