//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fabhotels.common.helper.common;

import com.fabhotels.common.helper.report.FileHelper;
import com.fabhotels.common.helper.report.ReportHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.xml.bind.JAXB;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.security.spec.KeySpec;
import java.util.*;
import java.util.Map.Entry;

public class ApiHelper {
    private static ReportHelper reporter = new ReportHelper();
    private static FileHelper fileHelper = new FileHelper();

    public ApiHelper() {
    }

    private static RequestSpecification prepareGetRequest(Map<String, String> queryParams) {
        RequestSpecification request = RestAssured.given();
        if (null != queryParams) {
            Iterator var2 = queryParams.entrySet().iterator();

            while(var2.hasNext()) {
                Entry<String, String> keyValue = (Entry)var2.next();
                request.param((String)keyValue.getKey(), new Object[]{keyValue.getValue()});
            }
        }

        return request;
    }

    public static Response getResponse(Map<String, String> queryParams, String url) {
        RequestSpecification request = prepareGetRequest(queryParams);
        Response response = (Response)request.get(url, new Object[0]);
        long responseTime = response.getTime();
        if (queryParams != null) {
            logRequestResponse(queryParams.toString().replace(", ", "&"), response.asString(), url, responseTime);
        } else {
            logRequestResponse((String)null, response.asString(), url, responseTime);
        }

        if (response.getStatusCode() != 200) {
            ReportHelper.logValidationFailure("HTTP Status code not 200 : " + Integer.toString(response.getStatusCode()), "200", Integer.toString(response.getStatusCode()), "HTTP status check failure");
            Reporter.log("Response has failed with HTTP status code - " + response.getStatusCode() + " & Response Message -" + response.asString());
            Assert.assertTrue(false);
        }

        return response;
    }

    public static Response getResponseWithHeaders(Map<String, String> queryParams, List<Header> headers, String url) {
        Headers requestHeader = new Headers(headers);
        RequestSpecification request = prepareGetRequest(queryParams);
        Response response = (Response)request.relaxedHTTPSValidation().headers(requestHeader).get(url, new Object[0]);
        System.out.println(response.getStatusCode()+"Satus code of the API");
        long responseTime = response.getTime();
        logRequestResponse(queryParams.toString().replace(", ", "&"), response.asString(), url, responseTime);
        if (response.getStatusCode() != 200 && response.getStatusCode() != 204) {
            ReportHelper.logValidationFailure("HTTP Status code not 200"+ Integer.toString(response.getStatusCode()), "200", Integer.toString(response.getStatusCode()), "HTTP status check failure");
            Reporter.log("Response has failed with HTTP status code - " + response.getStatusCode() + " & Response Message -" + response.asString());
            Assert.assertTrue(false);
        }

        return response;
    }

    public static Response postResponse(String body, String contentType, String url) {
        Response apiResp = (Response)RestAssured.given().contentType(contentType).body(body).post(url, new Object[0]);
        long responseTime = apiResp.getTime();
        logRequestResponse(body, apiResp.asString(), url, responseTime);
        if (apiResp.getStatusCode() != 200) {
            ReportHelper.logValidationFailure("HTTP Status code not 200"+ Integer.toString(apiResp.getStatusCode()), "200", Integer.toString(apiResp.getStatusCode()), "HTTP status check failure");
            Reporter.log("Response has failed with HTTP status code - " + apiResp.getStatusCode());
            Assert.assertTrue(false);
        }

        return apiResp;
    }

    public static Response postResponseWithHeaders(String body, List<Header> headers, ContentType contentType, String url) {
        Headers requestHeader = new Headers(headers);
        Response apiResp = (Response)RestAssured.given().relaxedHTTPSValidation().headers(requestHeader).contentType(contentType).body(body).post(url, new Object[0]);
        long responseTime = apiResp.getTime();
        System.out.println("Api response time ==>" + responseTime);
        apiResp.then().log().all();
        logRequestResponse(body, apiResp.asString(), url, responseTime);
        if (apiResp.getStatusCode() != 200) {
            ReportHelper.logValidationFailure("HTTP Status code not 200 : Response received ==> " + Integer.toString(apiResp.getStatusCode()), "200", Integer.toString(apiResp.getStatusCode()), "HTTP status check failure");
            Reporter.log("Response has failed with HTTP status code - " + apiResp.getStatusCode());
            Assert.assertTrue(false);
        }

        return apiResp;
    }

    public static void logRequestResponse(String request, String response, String url, long responseTime) {
        try {
            String requestFilePath = fileHelper.createRequestJsonFile(request, "");
            String responseFilePath = fileHelper.createResponseJsonFile(response, "");
            reporter.appendresultHTMLReport(reporter.getResultFileStringPath(), url, "<a href='" + requestFilePath + "'>Request Data</a>", "<a href='" + responseFilePath + "'>Response Data</a>", "Response Time(in msec) :- " + String.valueOf(responseTime), "", "");
        } catch (IOException var7) {
            var7.printStackTrace();
        }

    }
    public static void logRequest(String DriverInfo,String status, String message) {
        try {

            reporter.appendresultAppiumReport(reporter.getResultFileStringPath(),DriverInfo,status ,message);
        } catch (IOException var7) {
            var7.printStackTrace();
        }

    }

    public static String convertToJson(Object obj) {
        Gson gsonConverter = (new GsonBuilder()).disableHtmlEscaping().setPrettyPrinting().create();
        String jsonData = gsonConverter.toJson(obj);
        return jsonData;
    }

    public static Object convertFromJson(String jsonData, Class<?> classType) {
        Gson gsonConverter = (new GsonBuilder()).disableHtmlEscaping().setPrettyPrinting().create();
        Object objFromJson = gsonConverter.fromJson(jsonData, classType);
        return objFromJson;
    }

    public static String convertToXml(Object obj) {
        String xmlData = null;
        JAXB.marshal(obj, (String)xmlData);
        return (String)xmlData;
    }

    public static Object convertFromXml(String xmlData, Class<?> classType) {
        Object objFromXml = null;
        objFromXml = JAXB.unmarshal(xmlData, classType);
        return objFromXml;
    }

    public static String getValueOfProperty(String filePath, String keyName) {
        try {
            Properties prop = new Properties();
            InputStream inpStream = null;
            inpStream = new FileInputStream(filePath);
            prop.load(inpStream);
            return prop.getProperty(keyName);
        } catch (IOException var4) {
            Reporter.log("Filepath mentioned is not correct and method breaks with exception " + var4);
            return null;
        }
    }

    public static Set<String> getAllProperty(String filePath) {
        try {
            Properties prop = new Properties();
            InputStream inpStream = null;
            inpStream = new FileInputStream(filePath);
            prop.load(inpStream);
            return prop.stringPropertyNames();
        } catch (IOException var3) {
            Reporter.log("Filepath mentioned is not correct and method breaks with exception " + var3);
            return null;
        }
    }

    public static void setValueOfProperty(String filePath, String keyName, String value) {
        try {
            Properties prop = new Properties();
            FileInputStream in = new FileInputStream(filePath);
            prop.load(in);
            in.close();
            OutputStream outStream = null;
            outStream = new FileOutputStream(filePath);
            prop.setProperty(keyName, value);
            prop.store(outStream, (String)null);
            outStream.close();
        } catch (IOException var6) {
            Reporter.log("Filepath mentioned is not correct and method breaks with exception " + var6);
        }

    }

    public static boolean validateString(String strVal) {
        return strVal != null && !strVal.trim().isEmpty();
    }

    public static boolean verifyObject(JSONObject responseObj, String esFieldName) {
        if (responseObj.get(esFieldName) != null) {
            Reporter.log("<br/><font size='2'>ES Logs field " + esFieldName + " is null.</font>");
            return false;
        } else {
            return true;
        }
    }

    public boolean isStringFeildNotBlankInsideJSONObject(JSONObject jsonObject, String feild) {
        return !jsonObject.getString(feild).isEmpty() && jsonObject.getString(feild) != null;
    }

    public boolean isJsonObjectInsideJsonObjectNotBlank(JSONObject jsonObject, String feild) {
        return jsonObject.getJSONObject(feild) != null && jsonObject.getJSONObject(feild).length() != 0;
    }

    public boolean isJsonArrayNotEmpty(JSONArray jsonArray) {
        return jsonArray.length() != 0;
    }

    public boolean isJsonArrayNotBlank(JSONArray jsonArray) {
        return !jsonArray.equals((Object)null) && jsonArray.length() > 0;
    }

    public boolean isListFieldBlank(JSONObject jsonObject, String feild) {
        return jsonObject.getJSONArray(feild).length() != 0 && jsonObject.getJSONArray(feild) != null;
    }

    public static String generateAPIUrl(String configFile, String serverInitials, String endpoint) {
        String serverIp = getValueOfProperty(configFile, serverInitials);
        String serviceUrl;
        if (endpoint != null) {
            serviceUrl = serverIp + endpoint;
        } else {
            serviceUrl = serverIp;
        }

        return serviceUrl;
    }

    public static String[] getCommaSeperatedValues(String value) {
        return value.split(",");
    }

    public static boolean validateObject(Object obj) {
        return obj != null && obj != "";
    }

    public static String getValueFromXLResponse(String xmlResponse, String tagName, int index) {
        String value = "";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            InputSource inputSource = new InputSource(new StringReader(xmlResponse));
            Document doc = documentBuilder.parse(inputSource);
            NodeList list = doc.getElementsByTagName(tagName);
            System.out.println(list.item(index).getTextContent());
            value = list.item(index).getTextContent();
        } catch (ParserConfigurationException var9) {
            Reporter.log("Exception In processing XML Response");
        } catch (SAXException var10) {
            Reporter.log("Exception In processing XML Response");
        } catch (IOException var11) {
            Reporter.log("Exception In processing XML Response");
        }

        return value;
    }



    public String decryptAPIResponse(String key, String encrypted) {
        //System.out.println(key+"Printing Key");
        try {
            KeySpec keySpec = new DESKeySpec(key.getBytes("UTF-8"));

            SecretKeyFactory.getInstance("DES");

            SecretKey secretKey = SecretKeyFactory.getInstance("DES").generateSecret(keySpec);

            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(2, secretKey);

            String encryptedData= encrypted.replaceAll("\"", "");
            byte[] original = cipher.doFinal(java.util.Base64.getDecoder().decode(encryptedData));

            //System.out.println(new String(original, "UTF-8"));
            return new String(original, "UTF-8");
        } catch (Exception e) {
            throw new RuntimeException("Error in decrypting API response. Key = "+key+" Response = "+encrypted);
        }
    }
}
