//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fabhotels.common.helper.report;

import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import javax.crypto.SecretKey;
import java.io.*;
import java.util.UUID;

public class FileHelper {
    public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    byte[] keyAsBytes;
    SecretKey key;
    public static SoftAssert softAssert = new SoftAssert();

    public FileHelper() {
    }

    public void writeToFile(String str, String fileName, boolean appendToFile) {
        File file = null;
        file = new File(fileName);

        try {
            FileWriter writer = new FileWriter(file, appendToFile);
            writer.write(str);
            writer.close();
        } catch (IOException var6) {
            softAssert.assertTrue(false, "Error in writng to file..");
            var6.printStackTrace();
        }

    }

    public String getFileAbsolutePath(String partialFilePath, String filename) {
        String path = null;
        File file = new File(partialFilePath + "/" + filename);
        path = file.getAbsolutePath();
        return path;
    }

    public String readOutputFromFile(String fileName) {
        BufferedReader br = null;
        String finalStr = "";
        StringBuilder str = new StringBuilder();

        try {
            br = new BufferedReader(new FileReader(fileName));

            String sCurrentLine;
            while((sCurrentLine = br.readLine()) != null) {
                str.append(sCurrentLine);
            }
        } catch (IOException var14) {
            var14.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException var13) {
                var13.printStackTrace();
            }

        }

        finalStr = str.toString();
        return finalStr;
    }

    public String createRequestJsonFile(String jsonMsg, String fileName) throws FileNotFoundException {
        String requestFilePath = null;
        ReportHelper report = new ReportHelper();

        try {
            String resultFolder = report.getResultFolderpath();
            resultFolder = resultFolder + "/Requests";
            File resultFileFolder = new File(resultFolder);
            resultFileFolder.delete();
            resultFileFolder.mkdir();
            UUID randNo = UUID.randomUUID();
            requestFilePath = resultFolder + "/Requests_" + fileName + "_" + randNo + ".json";
            BufferedWriter resultFile = new BufferedWriter(new FileWriter(requestFilePath, true));
            resultFile.append(jsonMsg);
            resultFile.close();
        } catch (Exception var9) {
            Reporter.log("<br>" + var9.getMessage() + "<br>");
        }
        System.out.println(requestFilePath+"-------");
        requestFilePath = "." + requestFilePath.split(report.getResultFolderpath())[1];
        Reporter.log("<br><a href='" + requestFilePath + "'>Request_" + fileName + "</a><br>");
        return requestFilePath;
    }

    public String createResponseJsonFile(String jsonMsg, String fileName) throws FileNotFoundException {
        String responseFilePath = null;
        ReportHelper report = new ReportHelper();

        try {
            String resultFolder = report.getResultFolderpath();
            resultFolder = resultFolder + "/Responses";
            File resultFileFolder = new File(resultFolder);
            resultFileFolder.delete();
            resultFileFolder.mkdir();
            UUID randNo = UUID.randomUUID();
            responseFilePath = resultFolder + "/Responses_" + fileName + "_" + randNo + ".json";
            BufferedWriter resultFile = new BufferedWriter(new FileWriter(responseFilePath, true));
            resultFile.append(jsonMsg);
            resultFile.close();
        } catch (Exception var9) {
            Reporter.log("<br>" + var9.getMessage() + "<br>");
        }

        responseFilePath = "." + responseFilePath.split(report.getResultFolderpath())[1];
        Reporter.log("<br><a href='" + responseFilePath + "'>Response_" + fileName + "</a><br>");
        return responseFilePath;
    }

    public static void emptyFile(String path) {
        File file = new File(path);
        file.delete();

        try {
            file.createNewFile();
        } catch (IOException var3) {
            var3.printStackTrace();
        }

    }
}
