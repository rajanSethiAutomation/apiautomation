//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fabhotels.common.helper.report;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

public class ComparatorReportGenerator implements ReportConstants {
    public ComparatorReportGenerator() {
    }

    public void appendresultHTMLReport(String resultFileStringPath, String operationType, String fieldName, String liveValue, String qaValue) throws IOException {
        BufferedWriter resultFile = new BufferedWriter(new FileWriter(resultFileStringPath, true));
        resultFile.append("<tr>");
        resultFile.append("<td>" + operationType + "</td>");
        resultFile.append("<td>" + fieldName + "</td>");
        resultFile.append("<td>" + liveValue + "</td>");
        resultFile.append("<td>" + qaValue + "</td>");
        resultFile.append("</tr>");
        resultFile.close();
    }

    public String createCompartarReport() throws IOException {
        ReportHelper report = new ReportHelper();
        String resultFolder = report.getResultFolderpath();
        resultFolder = resultFolder + "/comparatorReport";
        File resultFileFolder = new File(resultFolder);
        resultFileFolder.mkdir();
        UUID randNo = UUID.randomUUID();
        String resultFileStringPath = resultFolder + "/compareResult_" + randNo + ".html";
        BufferedWriter resultFile = new BufferedWriter(new FileWriter(resultFileStringPath, true));
        resultFile.append("<html></head><body><title>Compare Engine Response</title><table border = '1'><tr><th>Operation Type</th><th>Field Name</th><th>Expected value</th><th>Test build value</th></tr>");
        resultFile.close();
        return resultFileStringPath;
    }

    public String createCompartarReportWithReq(String request) throws IOException {
        ApiReportHelper report = new ApiReportHelper();
        String resultFolder = report.getResultFolderpath();
        resultFolder = resultFolder + "/comparatorReport";
        File resultFileFolder = new File(resultFolder);
        resultFileFolder.mkdir();
        UUID randNo = UUID.randomUUID();
        String resultFileStringPath = resultFolder + "/compareResult_" + randNo + ".html";
        BufferedWriter resultFile = new BufferedWriter(new FileWriter(resultFileStringPath, true));
        resultFile.append("<html></head><body><title>Compare Engine Response</title><table border = '1'><tr><th>Operation Type</th><th>Field Name</th><th>Expected value</th><th>Test build value</th></tr>");
        resultFile.append("<tr><td><textarea disabled rows='7' cols='55'>REQUEST</textarea></td><td colspan='3'>" + request + "</td></tr>");
        resultFile.close();
        return resultFileStringPath;
    }

    public void appendFinalHTMLReport(String resultFileStringPath) throws IOException {
        BufferedWriter resultFile = new BufferedWriter(new FileWriter(resultFileStringPath, true));
        resultFile.append("</table></body></html>");
        resultFile.close();
    }
}
