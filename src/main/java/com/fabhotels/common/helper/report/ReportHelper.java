//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fabhotels.common.helper.report;

import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.io.*;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.fabhotels.Utilities.EmailReport.emailreport;

public class ReportHelper  implements ReportConstants {
    static int count = 0;
    static int passCount = 0;
    static int failCount = 0;
    static String newDate = null;
    static Date startTime = null;
    private static String resultFolderpath;
    private static String suiteFileStringPath;
    private static String resultFileStringPath;

    public static SoftAssert softAssert = new SoftAssert();


    public ReportHelper()  {
    }

    public String getResultFolderpath() {
        return resultFolderpath;
    }

    public void setResultFolderpath(String resultFolderpath) {
        resultFolderpath = resultFolderpath;
    }

    public String getResultFileStringPath() {
        return resultFileStringPath;
    }

    public void setResultFileStringPath(String resultFileStringPath) {
        resultFileStringPath = resultFileStringPath;
    }

    @BeforeSuite(
            alwaysRun = true
    )
    public void beforeSuite() throws IOException {
        String resultsFolderPath = "./Results";
        File resultFolder = new File(resultsFolderPath);
        if (resultFolder.delete()) {
            System.out.println("Content of folder deleted successfully");
        }

        resultFolder.mkdir();
        startTime = new Date();
        SimpleDateFormat oDateFormat = new SimpleDateFormat("dd_MM_YY");
        newDate = oDateFormat.format(startTime);
        resultFolderpath = resultsFolderPath + "/Result_" + newDate;
        File resultFileFolder = new File(resultFolderpath);
        resultFileFolder.mkdir();
        suiteFileStringPath = resultsFolderPath + "/API_FabHotels_TestResult_" + newDate + ".html";
        BufferedWriter resultFile = new BufferedWriter(new FileWriter(suiteFileStringPath, false));
        resultFile.append("<html><HEAD><TITLE>Automation Report</TITLE></HEAD><body><h4 align=\"center\"><FONT COLOR=\"660066\" FACE=\"Arial\"SIZE=5><b>API Automation Test Report</b>");
        resultFile.append("<h4 align='left'> <FONT COLOR=\"660000\" FACE=\"Arial\" SIZE=4.5> Detailed Report</h4><table  border=1 cellspacing=1  cellpadding=1 ><tr>");
        resultFile.append("<td width=150  align=\"center\" bgcolor=\"#e63d00\"><FONT COLOR=\"#E0E0E0\" FACE=\"Arial\" SIZE=2><b>Test Scenario</b></td>");
        resultFile.append("<td width=150  align=\"center\" bgcolor=\"#e63d00\"><FONT COLOR=\"#E0E0E0\" FACE=\"Arial\" SIZE=2><b>Test Scenario File</b></td>");
        resultFile.append("<td width=150  align=\"center\" bgcolor=\"#e63d00\"><FONT COLOR=\"#E0E0E0\" FACE=\"Arial\" SIZE=2><b>Status</b></td>");
        resultFile.close();
        this.setResultFolderpath(resultFolderpath);
    }

    @AfterSuite(
            alwaysRun = true
    )
    public void afterSuite() throws Exception {
        BufferedWriter resultFile = new BufferedWriter(new FileWriter(suiteFileStringPath, true));
        Date date = new Date();
        SimpleDateFormat oDateFormat = new SimpleDateFormat("dd/MMM/yyyy hh:mm:s");
        String sRunEndTime = oDateFormat.format(date);
        String sRunStartTime = oDateFormat.format(startTime);
        resultFile.append("</table>");
        resultFile.append("<h4 align='left'> <FONT COLOR=\"66000\" FACE=\"Arial\" SIZE=4.5> Summary</h4><table cellspacing=1 cellpadding=1 border=1>");
        resultFile.append("<tr><td width=300  align=\"center\" bgcolor=\"#e63d00\"><FONT COLOR=\"#E0E0E0\" FACE=\"Arial\" SIZE=2><b>Run Start Time</b></td>");
        resultFile.append("<td width=150 align=\"center\"><FONT COLOR=\"#385daa\" FACE=\"Arial\" SIZE=2.75><b>" + sRunStartTime + "</b></td></tr>");
        resultFile.append("<tr><td width=300  align=\"center\" bgcolor=\"#e63d00\"><FONT COLOR=\"#E0E0E0\" FACE=\"Arial\" SIZE=2><b>Run End Time</b></td>");
        resultFile.append("<td width=150 align=\"center\"><FONT COLOR=\"#385daa\" FACE=\"Arial\" SIZE=2.75><b>" + sRunEndTime + "</b></td></tr>");
        resultFile.append("<tr><td width=300  align=\"center\" bgcolor=\"#e63d00\"><FONT COLOR=\"#E0E0E0\" FACE=\"Arial\" SIZE=2><b>Total Tests executed</b></td>");
        resultFile.append("<td width=150 align=\"center\"><FONT COLOR=\"#385daa\" FACE=\"Arial\" SIZE=2.75><b>" + String.valueOf(passCount + failCount) + "</b></td></tr>");
        resultFile.append("<tr><td width=300  align=\"center\" bgcolor=\"#e63d00\"><FONT COLOR=\"#E0E0E0\" FACE=\"Arial\" SIZE=2><b>Total Pass Test count</b></td>");
        resultFile.append("<td width=150 align=\"center\"><FONT COLOR=\"#385daa\" FACE=\"Arial\" SIZE=2.75><b>" + String.valueOf(passCount) + "</b></td></tr>");
        resultFile.append("<tr><td width=300  align=\"center\" bgcolor=\"#e63d00\"><FONT COLOR=\"#E0E0E0\" FACE=\"Arial\" SIZE=2><b>Total Fail Test count</b></td>");
        resultFile.append("<td width=150 align=\"center\"><FONT COLOR=\"#385daa\" FACE=\"Arial\" SIZE=2.75><b>" + String.valueOf(failCount) + "</b></td></tr>");
        resultFile.append("</table></body></html>");
        resultFile.close();
        emailreport(suiteFileStringPath);

    }

    public void appendMethodInformationInSuite(String scenarioName, String methodName, String methodFilePath, String status) throws IOException {
        BufferedWriter resultFile = new BufferedWriter(new FileWriter(suiteFileStringPath, true));
        String statusTag = null;
        if (status.equalsIgnoreCase("Pass")) {
            statusTag = "<td bgcolor='#00FF7F'>";
        } else if (status.equalsIgnoreCase("Fail")) {
            statusTag = "<td bgcolor='#FF4500'>";
        } else {
            statusTag = "<td>";
        }

        resultFile.append("<tr>");
        resultFile.append("<td>" + scenarioName + "</td>");
        resultFile.append("<td><a href='" + methodFilePath + "' />" + methodName + "</td>");
        resultFile.append(statusTag + status + "</td>");
        resultFile.append("</tr>");
        resultFile.close();
    }

    @BeforeMethod(
            alwaysRun = true
    )
    public void createinitialHTMLReport(Method method) {
        try {
            UUID randNo = UUID.randomUUID();
            resultFileStringPath = resultFolderpath + "/" + method.getName().toString() + "_" + randNo + ".html";
            BufferedWriter resultFile = new BufferedWriter(new FileWriter(resultFileStringPath, true));
            resultFile.append("<html></head><body><title>API Test Report</title><table style=table-layout:fixed; width:40px border = '1'><tr><th>API URL</th><th>Request</th><th>Response</th><th>Message</th><th>Status</th><th>Calling Method</th></tr>");
            resultFile.close();
            this.setResultFileStringPath(resultFileStringPath);
        } catch (Exception var4) {
            resultFileStringPath = null;
        }

    }

    @AfterMethod(
            alwaysRun = true
    )
    public void appendFinalHTMLReport(ITestResult result) throws IOException {
        ITestNGMethod testNGMethod = result.getMethod();
        int number = testNGMethod.getCurrentInvocationCount();
        String status = "Pass";
        String scenario = "";
        if (result.getParameters().length > 0) {
            Object[] data = result.getParameters();

            for (int i = 0; i < result.getParameters().length; ++i) {
                System.out.println("Parameters are: " + data[i]);
            }
        }

        if (result.getStatus() == 2) {
            status = "Fail";
            ++failCount;
        } else {
            ++passCount;
        }

        ++count;
        String resultFilePath = resultFileStringPath.split("/Results")[1];
        resultFilePath = "." + resultFilePath;
        this.appendMethodInformationInSuite(scenario, result.getMethod().getMethodName() + "_" + number, resultFilePath, status);
        BufferedWriter resultFile = new BufferedWriter(new FileWriter(resultFileStringPath, true));
        resultFile.append("</table></body></html>");
        resultFile.close();

    }


    public void appendresultHTMLReport(String resultFileStringPath, String apiURL, String requestFile, String responseFile, String message, String status, String comment) throws IOException {
        BufferedWriter resultFile = new BufferedWriter(new FileWriter(resultFileStringPath, true));
        String statusTag = null;
        if (!status.equalsIgnoreCase("True") && !status.equalsIgnoreCase("Pass")) {
            if (!status.equalsIgnoreCase("false") && !status.equalsIgnoreCase("Fail")) {
                statusTag = "<td>";
            } else {
                statusTag = "<td bgcolor='#FF4500'>";
            }
        } else {
            statusTag = "<td bgcolor='#00FF7F'>";
        }

        resultFile.append("<tr>");
        resultFile.append("<td width=30% style=WORD-break:BREAK-all>" + apiURL + "</td>");
        resultFile.append("<td width=20% style=WORD-break:BREAK-all>" + requestFile + "</td>");
        resultFile.append("<td width=20% style=WORD-break:BREAK-all>" + responseFile + "</td>");
        resultFile.append("<td width=10% style=WORD-break:BREAK-all>" + message + "</td>");
        resultFile.append(statusTag + status + "</td>");
        resultFile.append("<td style=word-wrap: break-word>" + comment + "</td>");
        resultFile.append("</tr>");
        resultFile.close();
    }

    public void appendresultAppiumReport(String resultFileStringPath, String DriverInfo, String message, String status) throws IOException {
        BufferedWriter resultFile = new BufferedWriter(new FileWriter(resultFileStringPath, true));
        String statusTag = null;
        if (!status.equalsIgnoreCase("True") && !status.equalsIgnoreCase("Pass")) {
            if (!status.equalsIgnoreCase("false") && !status.equalsIgnoreCase("Fail")) {
                statusTag = "<td>";
            } else {
                statusTag = "<td bgcolor='#FF4500'>";
            }
        } else {
            statusTag = "<td bgcolor='#00FF7F'>";
        }

        resultFile.append("<tr>");
        resultFile.append("<td width=30% style=WORD-break:BREAK-all>" + DriverInfo + "</td>");
        resultFile.append("<td width=10% style=WORD-break:BREAK-all>" + status + "</td>");
        resultFile.append("<td width=20% style=WORD-break:BREAK-all>" + message + "</td>");
        resultFile.append(statusTag + status + "</td>");
        resultFile.append("</tr>");
        resultFile.close();
    }

    public void addDirToZipArchive(ZipOutputStream zos, File fileToZip, String parrentDirectoryName) throws Exception {
        if (fileToZip != null && fileToZip.exists()) {
            String zipEntryName = fileToZip.getName();
            if (parrentDirectoryName != null && !parrentDirectoryName.isEmpty()) {
                zipEntryName = parrentDirectoryName + "/" + fileToZip.getName();
            }

            int length;
            if (fileToZip.isDirectory()) {
                File[] var5 = fileToZip.listFiles();
                int var6 = var5.length;

                for (length = 0; length < var6; ++length) {
                    File file = var5[length];
                    this.addDirToZipArchive(zos, file, zipEntryName);
                }
            } else {
                byte[] buffer = new byte[1024];
                FileInputStream fis = new FileInputStream(fileToZip);
                zos.putNextEntry(new ZipEntry(zipEntryName));

                while ((length = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, length);
                }

                zos.closeEntry();
                fis.close();
            }

        }
    }

    public boolean reporterLogging(Boolean status, String sMessage) throws FileNotFoundException {
        ReportHelper reportHelper = new ReportHelper();

        try {
            reportHelper.appendresultHTMLReport(reportHelper.getResultFileStringPath(), "", "", "", sMessage, String.valueOf(status), Thread.currentThread().getStackTrace()[2].getMethodName());
            Reporter.log("<br>");
            if (status) {
                Reporter.log("<Font Color=#008000> PASS </Font>" + sMessage);
            } else {
                Reporter.log("<Font Color=red> FAIL </Font> " + sMessage);
            }
        } catch (Exception var5) {
            var5.getMessage();
        }

        return status;
    }

    public static void logValidationFailure(String fieldName, String expectedValue, String actualValue, String failureType)  {
        logValidationFailureWithRequest(fieldName, expectedValue, actualValue, failureType, (String) null);
    }

    public static void logValidationFailureWithRequest(String fieldName, String expectedValue, String actualValue, String failureType, String request)  {
        ReportHelper reporter = new ReportHelper();
        ComparatorReportGenerator compareHtmlObj = new ComparatorReportGenerator();
        String compareFilePath = null;
        String linkCompareFilePath = null;

        try {
            if (request == null) {
                compareFilePath = compareHtmlObj.createCompartarReport();
            } else {
                compareFilePath = compareHtmlObj.createCompartarReportWithReq(request);
            }

            linkCompareFilePath = "./comparatorReport/" + compareFilePath.split("comparatorReport")[1];
            compareHtmlObj.appendresultHTMLReport(compareFilePath, "<textarea disabled rows='7' cols='55'>" + failureType + "</textarea>", "<textarea disabled rows='7' cols='55'>" + fieldName + "</textarea>", "<textarea disabled rows='7' cols='55'>" + expectedValue + "</textarea>", "<textarea disabled rows='7' cols='55'>" + "</textarea>");
            compareHtmlObj.appendFinalHTMLReport(compareFilePath);
            String differenceHTMLLink = "<a href='" + linkCompareFilePath + "'>" + failureType + "</a>";
            reporter.reporterLogging(false, differenceHTMLLink);
        } catch (IOException var10) {
            var10.printStackTrace();
        }

    }

}
