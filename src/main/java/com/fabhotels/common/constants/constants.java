//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fabhotels.common.constants;

public interface constants {
    String FEASIBILITY_ES_LOGS_PATH = "http://10.5.198.56:8080/testFramework/rest/logger/findLogsById";
    String AIRTEL_COMMON_CONFIG = "airtelCommon.config";
    String CONFIG_FOLDER = "/app/digital/automation/config/";
    String LOG_NAME_KEY = "com.airtel.automation.log4j.property.path";

    interface ERROR_CODE {
        String CONFIGURATION_PROPERTY = "ERR_CFG_00";
    }
}
