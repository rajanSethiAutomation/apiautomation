package com.fabhotels.Controller;

import com.fabhotels.dto.request.Search.SearchProperties;

import java.util.ArrayList;
import java.util.List;

public class searchApi_popularCities_Request {


    public SearchProperties createSearchReuest(String lat, String Long, String distance, String minprice, String maxprice, String nearByRequired,
                                               String noOfPax, String sortOrder, String mealPlan, String preAppliedPriceRequired, String membershipLevel,
                                               String roomCount, String startDate, String endDate, String showAllProperties, String showInvisibleProperties, String requestType, String responseType, String deviceType, String pageType, String requestId, String sessionId, String dateLess, List<Integer> distanceBucket,List<Integer> localityid) {

        SearchProperties searchProperties_dto = new SearchProperties();//creating object of request pojo class

        searchProperties_dto.setDistance(Integer.valueOf(distance));
        searchProperties_dto.setDistanceBucket(distanceBucket);
        searchProperties_dto.setLocalityIds(localityid);
        searchProperties_dto.setNearByRequired(Boolean.valueOf(nearByRequired));
        searchProperties_dto.setNoOfPax(Integer.valueOf(noOfPax));
        searchProperties_dto.setPriceRangeMin(Integer.valueOf(minprice));
        searchProperties_dto.setPriceRangeMax(Integer.valueOf(maxprice));
        searchProperties_dto.setSortOrder(sortOrder);
        searchProperties_dto.setMealPlan(mealPlan);
        searchProperties_dto.setPreAppliedPriceRequired(Boolean.valueOf(preAppliedPriceRequired));
        searchProperties_dto.setMembershipLevel(Integer.valueOf(membershipLevel));
        searchProperties_dto.setRoomCount(Integer.valueOf(roomCount));
        searchProperties_dto.setStartDate(startDate);
        searchProperties_dto.setEndDate(endDate);
        searchProperties_dto.setShowAllProperties(Boolean.valueOf(showAllProperties));
        searchProperties_dto.setShowInvisibleProperties(Boolean.valueOf(showInvisibleProperties));
        searchProperties_dto.setRequestType(requestType);
        searchProperties_dto.setResponseType(responseType);
        searchProperties_dto.setDeviceType(deviceType);
        searchProperties_dto.setPageType(pageType);
        searchProperties_dto.setRequestId(requestId);
        searchProperties_dto.setSessionId(sessionId);
        searchProperties_dto.setDateLess(Boolean.valueOf(dateLess));


        return searchProperties_dto;


    }


    public List<Integer> distanceBucket() {
        List<Integer> distanceBucket = new ArrayList<>();
        distanceBucket.add(0);
        distanceBucket.add(1);
        distanceBucket.add(2);
        distanceBucket.add(3);
        distanceBucket.add(4);
        distanceBucket.add(5);
        distanceBucket.add(6);
        distanceBucket.add(7);

        return distanceBucket;
    }


    public List<Integer> LocalityID() {
        List<Integer> LocalityID = new ArrayList<>();
        LocalityID.add(142);
        return LocalityID;
    }


}

