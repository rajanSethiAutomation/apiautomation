package com.fabhotels.Controller;

import com.fabhotels.common.helper.report.ReportHelper;
import com.fabhotels.constants.EndPoints;
import com.fabhotels.constants.Headers;
import com.fabhotels.dto.request.Search.SearchProperties;
import com.fabhotels.dto.response.Search.AvgPoints;
import com.fabhotels.dto.response.Search.MinPoints;
import com.fabhotels.dto.response.Search.PsrDto;
import com.fabhotels.dto.response.Search.SearchPropertiesProperties;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.response.Response;
import org.testng.asserts.SoftAssert;

import java.util.List;

import static com.fabhotels.common.helper.common.ApiHelper.*;
import static com.fabhotels.common.helper.data.DbHelper.CONFIGURATION_FILE_PATH;

public class searchApi_DeviceType_business_Logic {

    SoftAssert softAssert = new SoftAssert();
    searchApi_Request searchAPI_request = new searchApi_Request();

    public Boolean validateTestCase(String First_Server, String Second_Server, String roomCount, String noOfPax, String startDate, String endDate, String distance,
                                    String showInvisibleProperties, String sortOrder, String requestType, String responseType,
                                    String devicetype, String pageType, String sessionID, String dateLess, String mealPlan, String membershipLevel) {

        List<Integer> distanceBucket = searchAPI_request.distanceBucket();
        List<String> propertyID = searchAPI_request.propertyID();

        SearchProperties generate_Request = searchAPI_request.createSearchReuest(roomCount, noOfPax, startDate, endDate, distance, distanceBucket, propertyID, showInvisibleProperties, sortOrder, requestType, responseType, devicetype, pageType, sessionID, dateLess, mealPlan, membershipLevel);
        System.out.println(convertToJson(generate_Request));

        Response getSearchResponse = get_Response(generate_Request, First_Server);

        Boolean check_Response = validateResponse(getSearchResponse, devicetype);

        if (!check_Response)
            ReportHelper.logValidationFailure("invalid  Response", "true", "false", "Invalid Response Failure");

        softAssert.assertAll();
        return check_Response;
    }

    public Boolean validateResponse(Response response, String devicetype) {

        Boolean result = false;


        SearchPropertiesProperties receivedResponse = (SearchPropertiesProperties) convertFromJson(
                response.asString(), SearchPropertiesProperties.class);

        if (receivedResponse.equals(null)) {
            ReportHelper.logValidationFailure("invalid  Response", "", "Received Response : " + receivedResponse.toString(), "Invalid Response Failure");

        }

        List<PsrDto> psrDtos = receivedResponse.getPsrDtos();

        for (PsrDto psrDto : psrDtos
                ) {

            //creating object of AvgPoints Class
            AvgPoints avgPoints = psrDto.getAvgPoints();
            MinPoints minPoints = psrDto.getMinPoints();

            result = avgPoints.getDeviceType().equals(devicetype) && minPoints.getDeviceType().equals(devicetype);

        }

        return result;
    }

    public Response get_Response(SearchProperties request, String serverName) {

        String serviceURL = generateAPIUrl(CONFIGURATION_FILE_PATH, serverName, EndPoints.searchProperty);
        System.out.println(serviceURL);
        //pass headers in Array List
        List<Header> headerList = Headers.fabhotels_Headers();
        String body = convertToJson(request);
        Response response = postResponseWithHeaders(body, headerList, ContentType.JSON, serviceURL);
        return response;


    }


}
