package com.fabhotels.Controller;

import com.fabhotels.common.helper.report.ReportHelper;
import com.fabhotels.constants.EndPoints;
import com.fabhotels.constants.Headers;
import com.fabhotels.dto.request.Search.SearchProperties;
import com.fabhotels.dto.response.Search.searchPopularCities.PsrDto;
import com.fabhotels.dto.response.Search.searchPopularCities.SearchPropertiesPopularCities;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.response.Response;
import org.apache.commons.collections4.CollectionUtils;
import org.testng.asserts.SoftAssert;

import java.util.Collections;
import java.util.List;

import static com.fabhotels.common.helper.common.ApiHelper.*;
import static com.fabhotels.common.helper.data.DbHelper.CONFIGURATION_FILE_PATH;

public class searchApi_PopularCities_business_Logic {

    SoftAssert softAssert = new SoftAssert();
    searchApi_popularCities_Request searchApi_popularCities_Request = new searchApi_popularCities_Request();

    public Boolean validateTestCase(String First_Server, String Second_Server, String lat, String Long, String distance, String minprice, String maxprice, String nearByRequired,
                                    String noOfPax, String sortOrder, String mealPlan, String preAppliedPriceRequired, String membershipLevel,
                                    String roomCount, String startDate, String endDate, String showAllProperties, String showInvisibleProperties, String requestType, String responseType, String deviceType, String pageType, String requestId, String sessionId, String dateLess) {

        List<Integer> distanceBucket = searchApi_popularCities_Request.distanceBucket();
        List<Integer> localityIDS = searchApi_popularCities_Request.LocalityID();

        SearchProperties generate_Request = searchApi_popularCities_Request.createSearchReuest(lat, Long, distance, minprice, maxprice, nearByRequired,
                noOfPax, sortOrder, mealPlan, preAppliedPriceRequired, membershipLevel,
                roomCount, startDate, endDate, showAllProperties, showInvisibleProperties, requestType, responseType, deviceType, pageType, requestId, sessionId, dateLess, distanceBucket, localityIDS);
        System.out.println(convertToJson(generate_Request));

        Response getSearchResponse = get_Response(generate_Request, First_Server);

        Boolean check_Response = validateResponse(getSearchResponse, localityIDS);

        if (!check_Response)
            ReportHelper.logValidationFailure("invalid  Response", "true", "false", "Invalid Response Failure");

        softAssert.assertAll();
        return true;
    }

    public Boolean validateResponse(Response response, List<Integer> localityIDS) {

        Boolean result = false;


        SearchPropertiesPopularCities receivedResponse = (SearchPropertiesPopularCities) convertFromJson(
                response.asString(), SearchPropertiesPopularCities.class);
        List<PsrDto> psrDtos = receivedResponse.getPsrDtos();

        for (PsrDto psrDto : psrDtos){


            List<Integer>localityids = psrDto.getMetaDataDto().getLocalityId();

            Boolean noElementsInCommon = CollectionUtils.containsAny(localityids, localityIDS);
            if (!noElementsInCommon) {
                result = false;
            } else result = true;

        }


        return result;
    }

    public Response get_Response(SearchProperties request, String serverName) {

        String serviceURL = generateAPIUrl(CONFIGURATION_FILE_PATH, serverName, EndPoints.searchProperty);
        System.out.println(serviceURL);
        //pass headers in Array List
        List<Header> headerList = Headers.fabhotels_Headers();
        String body = convertToJson(request);
        Response response = postResponseWithHeaders(body, headerList, ContentType.JSON, serviceURL);
        return response;


    }


}
