package com.fabhotels.Controller;

import com.fabhotels.common.helper.report.ReportHelper;
import com.fabhotels.constants.EndPoints;
import com.fabhotels.constants.Headers;
import com.fabhotels.dto.request.Search.SearchProperties;
import com.fabhotels.dto.response.Search.AvgPrice;
import com.fabhotels.dto.response.Search.PsrDto;
import com.fabhotels.dto.response.Search.SearchPropertiesProperties;
import com.fabhotels.dto.response.Search.searchFilterPolicy.SearchPropertiesFilterPolicy;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.response.Response;
import org.apache.commons.collections4.CollectionUtils;
import org.testng.asserts.SoftAssert;


import java.util.Collections;
import java.util.List;

import static com.fabhotels.common.helper.common.ApiHelper.*;
import static com.fabhotels.common.helper.data.DbHelper.CONFIGURATION_FILE_PATH;

public class searchApi_filterPolicy_business_Logic {

    SoftAssert softAssert = new SoftAssert();
    searchApi_filterPolicy_Request searchAPI_request = new searchApi_filterPolicy_Request();

    public Boolean validateTestCase(String First_Server,String Second_Server,String lat,String Long, String cityName, String Locality,String distance, String minprice, String maxprice, String nearByRequired,
                                    String noOfPax, String sortOrder, String mealPlan, String preAppliedPriceRequired, String membershipLevel,
                                    String roomCount, String startDate, String endDate, String showAllProperties, String showInvisibleProperties,String requestType , String responseType,String deviceType,String pageType,String requestId,String sessionId,String dateLess) {

        List<Integer> distanceBucket = searchAPI_request.distanceBucket();
        List<String> filterPolicy = searchAPI_request.filterPolicy();

        SearchProperties generate_Request = searchAPI_request.createSearchReuest(lat, Long, distance,cityName,Locality, minprice, maxprice, nearByRequired,
                noOfPax, sortOrder, mealPlan, preAppliedPriceRequired, membershipLevel,
                roomCount, startDate, endDate, showAllProperties, showInvisibleProperties, requestType, responseType, deviceType, pageType, requestId, sessionId, dateLess, distanceBucket,filterPolicy);
        System.out.println(convertToJson(generate_Request));

        Response getSearchResponse = get_Response(generate_Request, First_Server);

        Boolean check_Response = validateResponse(getSearchResponse, filterPolicy);

        if (!check_Response)
            ReportHelper.logValidationFailure("invalid  Response", "true", "false", "Invalid Response Failure");

        softAssert.assertAll();
        return check_Response;
    }

    public Boolean validateResponse(Response response, List<String> filterPolicy) {

        Boolean result = false;


        SearchPropertiesFilterPolicy receivedResponse = (SearchPropertiesFilterPolicy) convertFromJson(
                response.asString(), SearchPropertiesFilterPolicy.class);

       List<String> policyCodes = receivedResponse.getPoliciesCode();


        Boolean noElementsInCommon = CollectionUtils.containsAny(policyCodes,filterPolicy);

        if (!noElementsInCommon) {
            result = false;
        } else result = true;


        return result;
    }

    public Response get_Response(SearchProperties request, String serverName) {

        String serviceURL = generateAPIUrl(CONFIGURATION_FILE_PATH, serverName, EndPoints.searchProperty);
        System.out.println(serviceURL);
        //pass headers in Array List
        List<Header> headerList = Headers.fabhotels_Headers();
        String body = convertToJson(request);
        Response response = postResponseWithHeaders(body, headerList, ContentType.JSON, serviceURL);
        return response;


    }


}
