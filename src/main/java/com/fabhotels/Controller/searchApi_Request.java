package com.fabhotels.Controller;

import com.fabhotels.dto.request.Search.SearchProperties;

import java.util.ArrayList;
import java.util.List;

public class searchApi_Request {


    public SearchProperties createSearchReuest(String roomCount, String noOfPax, String startDate, String endDate, String distance, List<Integer> distanceBucket,
                                               List<String> propertiesID, String showInvisibleProperties, String sortOrder, String requestType, String responseType,
                                               String devicetype, String pageType, String sessionID, String dateLess, String mealPlan, String membershipLevel) {

        SearchProperties searchProperties_dto = new SearchProperties();//creating object of request pojo class
        searchProperties_dto.setRoomCount(Integer.valueOf(roomCount));
        searchProperties_dto.setNoOfPax(Integer.valueOf(noOfPax));
        searchProperties_dto.setStartDate(startDate);
        searchProperties_dto.setEndDate(endDate);
        searchProperties_dto.setDistance(Integer.valueOf(distance));
        searchProperties_dto.setDistanceBucket(distanceBucket);
        searchProperties_dto.setPropertyIds(propertiesID);
        searchProperties_dto.setShowInvisibleProperties(Boolean.valueOf(showInvisibleProperties));
        searchProperties_dto.setSortOrder(sortOrder);
        searchProperties_dto.setRequestType(requestType);
        searchProperties_dto.setResponseType(responseType);
        searchProperties_dto.setDeviceType(devicetype);
        searchProperties_dto.setPageType(pageType);
        searchProperties_dto.setSessionId(sessionID);
        searchProperties_dto.setDateLess(Boolean.valueOf(dateLess));
        searchProperties_dto.setMealPlan(mealPlan);
        searchProperties_dto.setMembershipLevel(Integer.valueOf(membershipLevel));


        return searchProperties_dto;
    }


    public List<Integer> distanceBucket(){
        List<Integer>  distanceBucket = new ArrayList<>();
        distanceBucket.add(0);
        distanceBucket.add(1);
        distanceBucket.add(2);
        distanceBucket.add(3);
        distanceBucket.add(4);
        distanceBucket.add(5);
        distanceBucket.add(6);
        distanceBucket.add(7);

        return distanceBucket;
    }



    public List<String> propertyID(){
        List<String>  propertyID = new ArrayList<>();
        propertyID.add("627");
        propertyID.add("896");
        propertyID.add("605");
        propertyID.add("786");

        return propertyID;
    }













}

