package com.fabhotels.Controller;

import com.fabhotels.common.helper.report.ReportHelper;
import com.fabhotels.constants.Headers;
import com.fabhotels.dto.request.Search.SearchProperties;
import com.fabhotels.dto.response.Search.SearchPropertiesProperties;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.response.Response;
import org.testng.asserts.SoftAssert;

import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.List;

import static com.fabhotels.common.helper.common.ApiHelper.*;
import static com.fabhotels.common.helper.data.DbHelper.CONFIGURATION_FILE_PATH;
import static com.fabhotels.constants.EndPoints.searchProperty;

public class searchApi_PropertyID_business_Logic {

    SoftAssert softAssert = new SoftAssert();
    searchApi_Request searchAPI_request = new searchApi_Request();

    public Boolean validateTestCase(String First_Server, String Second_Server, String roomCount, String noOfPax, String startDate, String endDate, String distance,
                                    String showInvisibleProperties, String sortOrder, String requestType, String responseType,
                                    String devicetype, String pageType, String sessionID, String dateLess, String mealPlan, String membershipLevel)  {

        List<Integer> distanceBucket = searchAPI_request.distanceBucket();
        List<String> propertyID = searchAPI_request.propertyID();

        SearchProperties generate_Request = searchAPI_request.createSearchReuest(roomCount, noOfPax, startDate, endDate, distance, distanceBucket, propertyID, showInvisibleProperties, sortOrder, requestType, responseType, devicetype, pageType, sessionID, dateLess, mealPlan, membershipLevel);
        System.out.println(convertToJson(generate_Request));

        Response getSearchResponse = get_Response(generate_Request, First_Server);

        Boolean check_Response = validateResponse(getSearchResponse, distanceBucket, propertyID);

        if (!check_Response)
            ReportHelper.logValidationFailure("invalid  Response", "true", "false", "Invalid Response Failure");


        return check_Response;
    }

    public Boolean validateResponse(Response response, List<Integer> distanceBucket, List<String> propertyID) {

        Boolean result = false;


        SearchPropertiesProperties receivedResponse = (SearchPropertiesProperties) convertFromJson(
                response.asString(), SearchPropertiesProperties.class);
        List<Integer> propertyIds = receivedResponse.getPropertyIds();


        Boolean noElementsInCommon = Collections.disjoint(propertyID, propertyIds);
        if (!noElementsInCommon) {
            result = false;
        } else result = true;

       // System.out.println(noElementsInCommon);


        softAssert.assertAll();
        return result;
    }

    public Response get_Response(SearchProperties request, String serverName)  {


        String serviceURL = generateAPIUrl(CONFIGURATION_FILE_PATH, serverName, searchProperty);

        System.out.println(serviceURL);

        //pass headers in Array List
        List<Header> headerList = Headers.fabhotels_Headers();

        String body = convertToJson(request);
        Response response = postResponseWithHeaders(body, headerList, ContentType.JSON, serviceURL);
        response.then().log().all();

        return response;


    }


}
