package com.fabhotels.Controller;

import com.fabhotels.common.helper.report.ReportHelper;
import com.fabhotels.constants.EndPoints;
import com.fabhotels.constants.Headers;
import com.fabhotels.dto.request.Search.SearchProperties;
import com.fabhotels.dto.response.Search.*;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.response.Response;
import org.testng.asserts.SoftAssert;

import java.util.List;

import static com.fabhotels.common.helper.common.ApiHelper.*;
import static com.fabhotels.common.helper.data.DbHelper.CONFIGURATION_FILE_PATH;

public class searchApi_Date_business_Logic {

    SoftAssert softAssert = new SoftAssert();
    searchApi_Request searchAPI_request = new searchApi_Request();

    public Boolean validateTestCase(String First_Server, String Second_Server, String roomCount, String noOfPax, String startDate, String endDate, String distance,
                                    String showInvisibleProperties, String sortOrder, String requestType, String responseType,
                                    String devicetype, String pageType, String sessionID, String dateLess, String mealPlan, String membershipLevel) {

        List<Integer> distanceBucket = searchAPI_request.distanceBucket();
        List<String> propertyID = searchAPI_request.propertyID();

        SearchProperties generate_Request = searchAPI_request.createSearchReuest(roomCount, noOfPax, startDate, endDate, distance, distanceBucket, propertyID, showInvisibleProperties, sortOrder, requestType, responseType, devicetype, pageType, sessionID, dateLess, mealPlan, membershipLevel);
        System.out.println(convertToJson(generate_Request));

        Response getSearchResponse = get_Response(generate_Request, First_Server);

        Boolean check_Response = validateResponse(getSearchResponse, propertyID, startDate);

        if (!check_Response)
            ReportHelper.logValidationFailure("invalid  Response", "true", "false", "Invalid Response Failure");

        softAssert.assertAll();
        return check_Response;
    }

    public Boolean validateResponse(Response response, List<String> propertyID, String startDate) {

        Boolean result = false;


        SearchPropertiesProperties receivedResponse = (SearchPropertiesProperties) convertFromJson(
                response.asString(), SearchPropertiesProperties.class);

        List<PsrDto> psrDtos = receivedResponse.getPsrDtos();

        for (PsrDto psrDto : psrDtos
                ) {

            //creating object of AvgPoints Class
            AvgPoints avgPoints = psrDto.getAvgPoints();
            //Creating object of Date Class of AvgPoints
            Date avgPointsDate = avgPoints.getDate();

            //Splitting the Request date in converting it into Integer
            String splitter[] = startDate.split("-");
            Integer year = Integer.valueOf(splitter[0]);
            Integer month = Integer.valueOf(splitter[1]);
            Integer day = Integer.valueOf(splitter[2]);

            //Validating the Expected and Actual results
            softAssert.assertEquals(year, avgPointsDate.getYear(), "Average Points Date year is not correct");
            softAssert.assertEquals(month, avgPointsDate.getMonth(), "Average Points Date month is not correct");
            softAssert.assertEquals(day, avgPointsDate.getDay(), "Average Points Date day is not correct");


            //creating object of MinPoints Class
            MinPoints minPrice = psrDto.getMinPoints();
            //Creating object of Date Class of MinPoints
            Date_ minPriceDate = minPrice.getDate();

            //Validating the Expected and Actual results
            softAssert.assertEquals(year, minPriceDate.getYear(), "Min Points Date year is not correct");
            softAssert.assertEquals(month, minPriceDate.getMonth(), "Min Points Date month is not correct");
            softAssert.assertEquals(day, minPriceDate.getDay(), "Min Points Date day is not correct");
            if (!minPriceDate.getYear().equals(0)) {
                return true;
            }
        }

        return result;
    }

    public Response get_Response(SearchProperties request, String serverName) {

        String serviceURL = generateAPIUrl(CONFIGURATION_FILE_PATH, serverName, EndPoints.searchProperty);
        System.out.println(serviceURL);
        //pass headers in Array List
        List<Header> headerList = Headers.fabhotels_Headers();
        String body = convertToJson(request);
        Response response = postResponseWithHeaders(body, headerList, ContentType.JSON, serviceURL);
        return response;


    }


}
