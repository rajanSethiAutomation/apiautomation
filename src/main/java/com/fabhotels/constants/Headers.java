package com.fabhotels.constants;

import io.restassured.http.Header;

import java.util.*;

public class Headers {



    public static List<Header> fabhotels_Headers() {

        List<Header> headerList = new ArrayList<>();
        headerList.add(new Header("Content-Type", "application/json"));
        headerList.add(new Header("Accept", "application/json"));

        return headerList;
    }
}



